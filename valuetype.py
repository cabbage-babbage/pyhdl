#from codenode import LiteralUIntNode

Q = "*"

class ValueType:
    def __init__(self):
        self.readable = False
        self.cur_writeable = False
        self.next_writeable = False
    def __str__(self):
        return repr(self)
    def copy_access(self, other):
        self.readable = other.readable
        self.cur_writeable = other.cur_writeable
        self.next_writeable = other.next_writeable
        return self
class UnknownType(ValueType):
    def __init__(self):
        super().__init__()
    def __hash__(self):
        return 0
    def __eq__(self, other):
        return isinstance(other, UnknownType)
    def copy(self):
        return UnknownType()
    def __repr__(self):
        return "UnknownType()"
class UInt(ValueType):
    def __init__(self, width=Q):
        super().__init__()
        self.width = width
        assert(width is Q or width >= 0)
    def __hash__(self):
        return hash(self.width)
    def __eq__(self, other):
        return isinstance(other, UInt) and other.width == self.width
    def copy(self):
        return UInt(self.width).copy_access(self)
    def of(self, value):
        if isinstance(value, str):
            if value.startswith("0b"):
                value = int(value[2:], base=2)
            elif value.startswith("0o"):
                value = int(value[2:], base=8)
            elif value.startswith("0d"):
                value = int(value[2:], base=10)
            elif value.startswith("0x"):
                value = int(value[2:], base=16)
            else:
                raise Exception("Could not convert string value '%s' to a UInt literal." % value)
        # hacky :(
        from codenode import LiteralUIntNode
        return LiteralUIntNode(self.width, value)
    def __repr__(self):
        return "UInt(%s)" % self.width
# assumed to be two's complement
class SInt(ValueType):
    def __init__(self, width=Q):
        super().__init__()
        self.width = width
        assert(width is Q or width >= 0)
    def __hash__(self):
        return hash(self.width)
    def copy(self):
        return SInt(self.width).copy_access(self)
    def __eq__(self, other):
        return isinstance(other, SInt) and other.width == self.width
    def __repr__(self):
        return "SInt(%s)" % self.width
class Vec(ValueType):
    def __init__(self, etype, width=Q):
        super().__init__()
        self.etype = etype
        self.width = width
        assert(width is Q or width >= 0)
    def __hash__(self):
        return hash((self.width, self.etype))
    def __eq__(self, other):
        return isinstance(other, Vec) and other.etype == self.etype and other.width == self.width
    def copy(self):
        return Vec(self.etype.copy(), self.width).copy_access(self)
    def __repr__(self):
        return "Vec(%s, %s)" % (self.etype, self.width)
class Struct(ValueType):
    # TODO: rename 'elems' -> 'fields'
    def __init__(self, **kwargs):
        super().__init__()
        self.elems = kwargs
    def __hash__(self):
        return hash(tuple(self.elems.values()))
    def __eq__(self, other):
        return isinstance(other, Struct) and other.elems == self.elems
    def copy(self):
        return Struct(**{ name: self.elems[name].copy() for name in self.elems }).copy_access(self)
    def __repr__(self):
        return "Struct(%s)" % ", ".join(["%s=%s" % (key, self.elems[key]) for key in self.elems])
