import functools
from module import Module
from modulestack import *
from codenode import *

def Input(vtype, name="<input>"):
    if exists_top_module():
        top_module().add_input(vtype, name)
    return InputNode(vtype, name)
def Wire(vtype, name="<wire>"):
    if exists_top_module():
        top_module().add_wire(vtype, name)
    return WireNode(vtype, name)
def Register(vtype, reset=None, name="<reg>"):
    assert(reset is None or isinstance(reset, LiteralNode))
    if exists_top_module():
        if reset is None:
            reset = DefaultValueNode(vtype)
        top_module().add_register(vtype, reset, name)
    return RegisterNode(vtype, name)
def Output(vtype, name="<output>"):
    if exists_top_module():
        top_module().add_output(vtype, name)
    return OutputNode(vtype, name)
def Instance(compiled_module, name="<instance>"):
    if exists_top_module():
        top_module().add_instance(compiled_module, name)
    return InstanceNode(compiled_module, name)

def cat(val1, val2):
    return CatNode(val1, val2)
def mux(sel, *inputs):
    return MuxNode(sel, inputs)

def module(module_def):
    # TODO: cache based on kwargs as well
    # cache is (name+args) -> CompiledModule
    cache = {}
    @functools.wraps(module_def)
    def wrapper(*args, **kwargs):
        if kwargs:
            raise Exception("No kwargs accepted right now :(")
        cache_key = (module_def.__name__,) + args
        compiled_module = None
        if cache_key in cache:
            #print("using cached module of %s(%s)" % (module_def.__name__, tuple(args)))
            compiled_module = cache[cache_key]
        else:
            #print("defining new module %s(%s)" % (module_def.__name__, tuple(args)))
            module_name = module_def.__name__
            if args:
                module_name += "(%s)" % ", ".join(str(arg) for arg in args)
            module = Module(module_name)
            push_module(module)

            try:
                set_structural_equality()

                module.start_execution()
                module_def(*args, **kwargs)
                module.end_execution()
                while module.keep_executing():
                    module.start_execution()
                    module_def(*args, **kwargs)
                    module.end_execution()

                module.start_collect_components()
                module_def(*args, **kwargs)
                module.end_collect_components()

                module.compile()
                compiled_module = module.compiled_module()
                compiled_module.typecheck()
                #print("done defining new module %s(%s)" % (module_def.__name__, tuple(args)))
                cache[cache_key] = compiled_module
            finally:
                pop_module()
            
        return compiled_module

    return wrapper
