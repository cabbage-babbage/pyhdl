from compiledmodule import CompiledModule
from codenode import *
from valuetype import *

class SimValue:
    def __init__(self):
        pass
class UnassignedSimValue(SimValue):
    def __init__(self):
        pass
    def __str__(self):
        return "X"
class UIntSimValue(SimValue):
    def __init__(self, width, value):
        assert(width >= 0)
        assert(0 <= value < 2**width)
        self.width = width
        self.value = value
    def __str__(self):
        return "UInt<%d>(%d)" % (self.width, self.value)
class VecSimValue(SimValue):
    def __init__(self, width, values):
        assert(width >= 0)
        assert(len(values) == width)
        self.width = width
        self.values = values
    def __str__(self):
        return "Vec<%d>(%s)" % (self.width, ", ".join(str(value) for value in self.values))
class StructSimValue(SimValue):
    def __init__(self, values):
        self.values = values
    def __str__(self):
        return "{ %s }" % ", ".join("%s: %s" % kvp for kvp in self.values.iteritems()) if self.values else "{}"

class ModuleSim:
    def __init__(self, module):
        assert(isinstance(module, CompiledModule))
        assert(module.typechecked)
        self.module = module
        self.reg_state = { }
        self.init_default_reg_state(module)
    def init_default_reg_state(self, module):
        for elem in module.elements.values():
            if isinstance(elem, RegisterNode):
                reg = elem
                self.reg_state[reg] = self.value_of_literal(reg.reset)
        for submodule in module.submodules:
            self.init_default_reg_state(submodule)
    def value_of_literal(self, lit):
        if isinstance(lit, DefaultValueNode):
            return self.default_value(lit.vtype)
        elif isinstance(lit, LiteralUIntNode):
            return UIntSimValue(lit.width, lit.value)
        else:
            raise NotImplemented()
    def default_value(self, vtype):
        if isinstance(vtype, UInt):
            return UIntSimValue(vtype.width, 0)
        elif isinstance(vtype, Vec):
            elem_value = self.default_value(vtype.etype)
            return VecSimValue(vtype.width, [elem_value] * vtype.width)
        elif isinstance(vtype, Struct):
            values = { }
            for name in vtype.elems:
                values[name] = self.default_value(vtype.elems[name])
            return StructSimValue(values)
        else:
            raise NotImplemented()

    def eval_expr(self, elem_state, expr):
        raise NotImplemented

    def run_code(self, elem_state, next_reg_state, code):
        raise NotImplemented

    # returns dict of outputs, and updates internal register state
    def run_cycle(self, inputs):
        elem_state = dict(self.reg_state)
        next_reg_state = dict(self.reg_state)
        for name in inputs:
            elem_state[name] = inputs[name]
        
        for node in self.module.code:
            pass
