from valuetype import * # get UInt, Vec, etc.
from modulefn import * # get Input, Output, cat, mux, etc.

# add n-bit input with m-bit input
@module
def delayed_adder(n, m):
    a = Input(UInt(n), name="a")
    b = Input(UInt(m), name="b")
    #reg = Register(UInt(n + m + 1), name="reg")
    reg = Register(UInt(), name="reg")
    #out = Output(UInt(n + m + 1), name="out")
    out = Output(UInt(), name="out")
    reg.next = a + b
    out.cur = reg

# n-bit selector between m-bit inputs
@module
def select(n, m):
    inputs = Input(Vec(UInt(m), 2**n), name="in")
    selector = Input(UInt(n), name="sel")
    #out = Output(UInt(m), name="out")
    out = Output(UInt(), name="out")
    for i in range(2**n):
        with selector == UInt().of(i):
            out.cur = inputs[i]

# n-bit selector between m-bit inputs
@module
def rec_select(n, m):
    inputs = Input(Vec(UInt(m), 2**n), name="in")
    selector = Input(UInt(n), name="sel")
    out = Output(UInt(m), name="out")
    #out = Output(UInt(), name="out")
    if n == 0:
        out.cur = inputs[0]
    else:
        select_lo = Instance(rec_select(n - 1, m), name="rec_select_lo")
        select_hi = Instance(rec_select(n - 1, m), name="rec_select_hi")
        select_lo.io["in"].cur = inputs[:2**(n-1)]
        select_lo.io.sel.cur = selector[:-1]
        select_hi.io["in"].cur = inputs[2**(n-1):]
        select_hi.io.sel.cur = selector[:-1]
        if selector[-1]:
            out.cur = select_hi.io.out
        else:# ~selector[-1]:
            out.cur = select_lo.io.out

@module
def test_mod(n):
    a = Input(UInt(n), name="a")
    reg = Register(UInt(n), name="reg")
    out = Output(UInt(n), name="out")
    reg.next = reg + a
    if a == UInt(n).of(0):
        reg.next = UInt().of(0)
    out.cur = reg

@module
def test_mod2(n):
    a = Input(UInt(n), name="a")
    reg = Register(UInt(n), name="reg")
    out = Output(UInt(n), name="out")
    reg.next = reg + a
    with a == UInt(n).of(0):
        reg.next = UInt().of(0)
    out.cur = reg

@module
def test_mod3(n):
    a = Input(UInt(n), name="a")
    reg = Register(UInt(n), name="reg")
    out = Output(UInt(n), name="out")
    reg.next = mux(a == UInt().of(0), UInt().of(0), reg + a)
    out.cur = reg

@module
def delay(time, width):
    a = Input(UInt(width), name="in")
    #out = Output(UInt(width), name=("out-%d"%time))
    out = Output(UInt(), name="out")
    if time == 0:
        out.cur = a
    else:
        sub_delay = Instance(delay(time - 1, width), name="delay_%d"%(time-1))
        #reg = Register(UInt(width), name=("reg-%d"%time))
        reg = Register(UInt(), name=("reg-%d"%time))

        sub_delay.io["in"].cur = a
        reg.next = sub_delay.io.out
        out.cur = reg

@module
def weird_adder(log_in, width):
    inputs = [Input(UInt(width), name="in:%d"%i) for i in range(2**log_in)]
    #output = Output(UInt(width + log_in), name="out")
    output = Output(UInt(), name="out")
    if log_in == 1:
        #cond = (inputs[0] == UInt(width).of(0)) | (inputs[1] == UInt(width).of(0))
        cond = (inputs[0] == UInt().of(0)) | (inputs[1] == UInt().of(0))
        with cond:
            #output.cur = UInt(width + log_in).of(0)
            output.cur = UInt().of(0)
        with ~cond:
            output.cur = inputs[0] + inputs[1]
        #if (inputs[0] == UInt(width).of(0)) | (inputs[1] == UInt(width).of(0)):
        #    output.cur = UInt(width).of(0)
        #else:
        #    output.cur = inputs[0] + inputs[1]
    else:
        sub_adder_0 = Instance(weird_adder(log_in - 1, width), "adder_0")#"adder_0(%d,%d)" % (log_in-1, width))
        sub_adder_1 = Instance(weird_adder(log_in - 1, width), "adder_1")#"adder_1(%d,%d)" % (log_in-1, width))
        for i in range(2**(log_in-1)):
            sub_adder_0.io["in:%d"%i].cur = inputs[i]
        for i in range(2**(log_in-1)):
            sub_adder_1.io["in:%d"%i].cur = inputs[i + 2**(log_in-1)]
        output.cur = sub_adder_0.io.out + sub_adder_1.io.out

@module
def weird_adder2(log_in, width):
    inputs = Input(Vec(UInt(width), 2**log_in), name="in")
    output = Output(UInt(), name="out")
    if log_in == 1:
        if (inputs[0] == UInt().of(0)) | (inputs[1] == UInt().of(0)):
            output.cur = UInt().of(0)
        else:
            output.cur = inputs[0] + inputs[1]
    else:
        sub_adder_0 = Instance(weird_adder2(log_in - 1, width), "adder_0")
        sub_adder_1 = Instance(weird_adder2(log_in - 1, width), "adder_1")
        sub_adder_0.io["in"].cur = inputs[:2**(log_in-1)]
        sub_adder_1.io["in"].cur = inputs[2**(log_in-1):]
        output.cur = sub_adder_0.io.out + sub_adder_1.io.out

@module
def ram(addr_bits, width):
    en = Input(UInt(1), name="en")
    addr = Input(UInt(addr_bits), name="addr")
    w_en = Input(UInt(1), name="w_en")
    data = Input(UInt(width), name="data")
    out = Output(UInt(width), name="out")

    memory = Register(Vec(UInt(width), 2**addr_bits), name="memory")

    with en:
        with w_en:
            memory[addr].next = data
        with ~w_en:
            out.cur = memory[addr]

@module
def tiny_cpu():
    addr = Output(UInt(32), name="addr")
    inst = Input(Struct(op=UInt(4), rs1=UInt(4), rs2=UInt(4), rd=UInt(4), imm=UInt(16)), name="inst")
    pc = Register(UInt(32), name="pc")

    addr.cur = pc
    pc.next = pc + UInt().of(4)

    regfile = Register(Vec(UInt(32), 16), name="regfile")

    # DECODE
    rs1_val = Wire(UInt(), name="rs1_val")
    rs2_val = Wire(UInt(), name="rs2_val")

    rs1_val.cur = regfile[inst["rs1"]]
    rs2_val.cur = regfile[inst["rs2"]]

    sext_imm = Wire(UInt(32), name="sext_imm")
    if inst["imm"][15]: 
        sext_imm.cur = cat(UInt().of("0xFFFF"), inst["imm"])
    else:
        sext_imm.cur = cat(UInt().of("0x0000"), inst["imm"])

    # EXECUTE
    result = Wire(UInt(32), name="result")
    branch = Wire(UInt(1), name="branch")
    wb = Wire(UInt(1), name="wb")
    result.cur = UInt().of(0)
    branch.cur = UInt().of(0)
    wb.cur = UInt().of(0)

    # add
    with inst["op"] == UInt().of(0):
        result.cur = rs1_val + rs2_val
        wb.cur = UInt().of(1)

    # addi
    with inst["op"] == UInt().of(1):
        result.cur = rs1_val + sext_imm
        wb.cur = UInt().of(1)

    # sub
    with inst["op"] == UInt().of(2):
        result.cur = rs1_val - rs2_val
        wb.cur = UInt().of(1)

    # and
    with inst["op"] == UInt().of(3):
        result.cur = rs1_val & rs2_val
        wb.cur = UInt().of(1)

    # or
    with inst["op"] == UInt().of(4):
        result.cur = rs1_val | rs2_val
        wb.cur = UInt().of(1)

    # sll
    with inst["op"] == UInt().of(5):
        result.cur = rs1_val << rs2_val
        wb.cur = UInt().of(1)

    # slr
    with inst["op"] == UInt().of(6):
        result.cur = rs1_val >> rs2_val
        wb.cur = UInt().of(1)

    # beq
    with inst["op"] == UInt().of(7):
        result.cur = pc + sext_imm
        branch.cur = rs1_val == rs2_val

    # blt
    with inst["op"] == UInt().of(8):
        result.cur = pc + sext_imm
        branch.cur = rs1_val < rs2_val

    # j
    with inst["op"] == UInt().of(9):
        result.cur = pc + sext_imm
        branch.cur = UInt().of(1)

    # jr
    with inst["op"] == UInt().of(10):
        result.cur = rs1_val
        branch.cur = UInt().of(1)

    # WRITEBACK
    with branch:
        pc.next = result

    with wb:
        regfile[inst["rd"]].next = result

    regfile[0].next = UInt().of(0)

@module
def processor_system():
    mem = Instance(ram(32, 32), name="mem")
    cpu = Instance(tiny_cpu(), name="cpu")

    mem.io.en.cur = UInt(1).of(1)
    mem.io.w_en.cur = UInt(1).of(0)
    mem.io.data.cur = UInt().of(0) # just to remove warning
    mem.io.addr.cur = cpu.io.addr
    inst_bits = mem.io.out
    inst = cpu.io.inst
    inst.op .cur = inst_bits[ 0: 4]
    inst.rs1.cur = inst_bits[ 4: 8]
    inst.rs1.cur = inst_bits[ 8:12]
    inst.rd .cur = inst_bits[12:16]
    inst.imm.cur = inst_bits[16:32]

@module
def test_mod4(n):
    a = Input(UInt(n), name="a")
    reg = Register(UInt(n), name="reg")
    out = Output(UInt(), name="out")
    w = Wire(UInt(n - 1), name="w") # this is unsat, since n-1 < n
    reg.next = reg + a
    with a == UInt(n).of(0):
        reg.next = UInt().of(0)
    out.cur = reg
    w.cur = out

def nbits(value):
    bits = 0
    while value > 0:
        value = value // 2
        bits += 1
    return bits

# FIFO buffer of depth n, storing values whose type is etype
@module
def fifo(n, etype):
    data_in = Input(etype, name="data_in")
    w_en = Input(UInt(1), name="w_en")
    r_en = Input(UInt(1), name="r_en")
    data_out = Output(etype, name="data_out")
    full = Output(UInt(1), name="full")
    empty = Output(UInt(1), name="empty")

    buf = Register(Vec(etype, n), name="buf")
    # top is always the next value to write
    top = Register(UInt(nbits(n)), name="top") # big enough to point inside the buf!

    empty.cur = top == UInt().of(0)
    full.cur = top == UInt().of(n)
    
    # assert(not (full and w_en) and not (empty and r_en))

    r = r_en & ~empty
    w = w_en & ~full

    with (r_en & ~empty) & ~w_en:
        top.next = top - UInt().of(1)
        data_out.cur = buf[top.next]
    with (w_en & ~full) & ~r_en:
        top.next = top + UInt().of(1)
        buf[top.next].next = data_in
    with (r_en & ~empty) & w_en:
        # just replace the current top value
        data_out.cur = buf[top - UInt().of(1)]
        buf[top - UInt().of(1)].next = data_in

@module
def cycle_testing():
    w = Wire(UInt(10), name="w")
    w.cur = ~w

@module
def cycle_testing2a():
    o = Output(UInt(10), name="o")
    o.cur = UInt().of(0)
@module
def cycle_testing2b():
    o = Output(UInt(10), name="o")
    t2a = Instance(cycle_testing2a(), name="cycle_testing_2a")
    o.cur = t2a.io.o

@module
def inf_testing_a():
    a = Input(UInt(), name="a")
@module
def inf_testing_b():
    m1 = Instance(inf_testing_a(), name="m1")
    m2 = Instance(inf_testing_a(), name="m2")
    m1.io.a.cur = UInt(10).of(0)
    m2.io.a.cur = UInt(20).of(0)

@module
def cycle_testing3(n):
    a = Input(UInt(1), name="a")
    out = Output(UInt(1), name="out")
    if n == 0:
        out.cur = a
    else:
        inst1 = Instance(cycle_testing3(n - 1), "inst1")
        inst2 = Instance(cycle_testing3(n - 1), "inst2")
        inst1.io.a.cur = a
        inst2.io.a.cur = a
        out.cur = inst1.io.out + inst2.io.out

@module
def io_testing(n):
    a = Input(UInt(1), name="a")
    out = Output(UInt(1), name="out")
    if n == 0:
        out.cur = a
    else:
        inst1 = Instance(cycle_testing3(n - 1), "inst1")
        inst2 = Instance(cycle_testing3(n - 1), "inst2")
        inst1.io.a.cur = a
        inst2.io.a.cur = a
        out.cur = inst1.io.out + inst2.io.out

@module
def more_inf_testing():
	w = Wire(Vec(UInt(1), 2), name="w")
	w[0].cur = UInt(1).of(5)
	w[1].cur = w[0]

@module
def more_cycle_testing():
	w = Wire(Vec(UInt(3), 2), name="w")
	w[0].cur = UInt().of(5)
	w[1].cur = w[0]

def all_the_modules():
    return [
        delayed_adder(5, 10),
        select(3, 16),
        rec_select(3, 16),
        test_mod(14),
        test_mod2(14),
        test_mod3(14),
        delay(3, 20),
        weird_adder(3, 32),
        weird_adder2(3, 32),
        ram(32, 32),
        tiny_cpu(),
        processor_system(),
        test_mod4(7),
        fifo(5, Struct(a=Vec(UInt(10), 20))),
        cycle_testing(),
        cycle_testing2a(),
        cycle_testing2b(),
        cycle_testing3(3),
        inf_testing_a(),
        inf_testing_b(),
        io_testing(3),
        more_inf_testing(),
        more_cycle_testing()
        ]

def do_test(m):
    print("TESTING MODULE %s" % m.name)
    try:
        m.typecheck()
        inst = m.as_full_instance()
        try:
            inst.infer_widths()
        except Exception as e:
            print("Width inference error: %s" % e)
        try:
            inst.verify_no_cycles()
        except Exception as e:
            print("Cycle verification error: %s" % e)
    except Exception as e:
        print("Type error: %s" % e)

def unit_test(m, typecheck, widths, no_cycles):
    if typecheck == True:
        assert(widths is not None)
    elif typecheck == False:
        assert(widths is None and no_cycles is None)
    if widths == True:
        assert(no_cycles is not None)
    else:
        assert(no_cycles is None)
    passed = True
    try:
        m.typecheck()
        if typecheck == False:
            print("typecheck: failure (expected to not typecheck, but typechecking completed")
            passed = False
        else:
            inst = m.as_full_instance()
            try:
                inst.infer_widths()
                if widths == False:
                    print("width inference: failure (expected to not infer widths, but width inference completed)")
                    passed = False
                try:
                    inst.verify_no_cycles()
                    if no_cycles == False:
                        print("no-cycle verification: failure (expected to detect cycles, but no-cycle verification completed)")
                        passed = False
                except Exception:
                    if no_cycles == True:
                        print("no-cycle verification: failure (expected to verify no cycles, but did not)")
                        passed = False
            except Exception:
                if widths == True:
                    print("width inference: failure (expected to infer widths, but could not)")
                    passed = False
    except Exception:
        if typecheck == True:
            print("typecheck: failure (expected to typecheck, but did not typecheck)")
            passed = False
    return passed

def test_all():
    failed = []
    def test(m, typecheck, widths, no_cycles):
        if not unit_test(m, typecheck, widths, no_cycles):
            print("**** UNIT TEST FAILURE: module %s" % m.name)
            failed.append(m)

    test(delayed_adder(5, 10),                  typecheck=True, widths=True,  no_cycles=True)
    test(select(3, 16),                         typecheck=True, widths=True,  no_cycles=True)
    test(rec_select(3, 16),                     typecheck=True, widths=True,  no_cycles=True)
    test(test_mod(14),                          typecheck=True, widths=True,  no_cycles=True)
    test(test_mod2(14),                         typecheck=True, widths=True,  no_cycles=True)
    test(test_mod3(14),                         typecheck=True, widths=True,  no_cycles=True)
    test(delay(3, 20),                          typecheck=True, widths=True,  no_cycles=True)
    test(weird_adder(3, 32),                    typecheck=True, widths=True,  no_cycles=True)
    test(weird_adder2(3, 32),                   typecheck=True, widths=True,  no_cycles=True)
    # TODO: make blackbox memory module so we can properly test this without implementing ram as a vector
    #test(ram(32, 32),                           typecheck=True, widths=True,  no_cycles=True)
    test(ram(10, 32),                           typecheck=True, widths=True,  no_cycles=True)
    test(tiny_cpu(),                            typecheck=True, widths=True,  no_cycles=True)
    # TODO: make blackbox memory module so we can properly test this without implementing ram as a vector
    #test(processor_system(),                    typecheck=True, widths=True,  no_cycles=True)
    test(test_mod4(7),                          typecheck=True, widths=False, no_cycles=None)
    test(fifo(5, Struct(a=Vec(UInt(10), 20))),  typecheck=True, widths=True,  no_cycles=True)
    test(cycle_testing(),                       typecheck=True, widths=True,  no_cycles=False)
    test(cycle_testing2a(),                     typecheck=True, widths=True,  no_cycles=True)
    test(cycle_testing2b(),                     typecheck=True, widths=True,  no_cycles=True)
    test(cycle_testing3(3),                     typecheck=True, widths=True,  no_cycles=True)
    test(inf_testing_a(),                       typecheck=True, widths=False, no_cycles=None)
    test(inf_testing_b(),                       typecheck=True, widths=True,  no_cycles=True)
    test(io_testing(3),                         typecheck=True, widths=True,  no_cycles=True)
    test(more_inf_testing(),                    typecheck=True, widths=False, no_cycles=None)
    test(more_cycle_testing(),                  typecheck=True, widths=True,  no_cycles=True)

    if failed:
        print("==============================================")
        print("The following modules did not pass unit tests:")
        for m in failed:
            print("   %s" % m.name)
    else:
        print("======================")
        print("Unit tests all passed.")

def print_edges(m):
    for edge in m.edge_ordering:
        print("'%s': <%s> <= <%s>" % (edge.ref, edge.dst.ref, edge.src.ref))
