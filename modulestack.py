module_stack = []
def push_module(module):
    module_stack.append(module)
def top_module():
    assert(exists_top_module())
    return module_stack[-1]
def pop_module():
    global module_stack
    module, module_stack = module_stack[-1], module_stack[:-1]
    return module
def exists_top_module():
    return len(module_stack) > 0

