from codenode import *
from compiledmodule import CompiledModule, InputComponent, WireComponent, RegisterComponent, OutputComponent, ThinModuleInstance

phase_none = "phase:none"
phase_finding_structure = "phase:finding-structure"
phase_collecting_components = "phase:collecting-components"
phase_collected_components = "phase:collected-components"
phase_compiling_traces = "phase:compiling-traces"
phase_compiled = "phase:compiled"

class Module:
    def __init__(self, name):
        self.name = name
        self.code = []
        self.cur_branches = []
        self.cur_branch_idx = None
        self.cur_trace = None
        self.cur_withstack = None
        self.traces = {} # map: [branches] -> trace
        self.phase = phase_none
        self.components = {}
        self.thin_instances = {}

    # should node operations be symbolic right now or not?
    def is_symbolic(self):
        # when finding structure, we definitely need operations to be symbolic.
        # when collecting components, we only want operations to be symbolic so that no errors are thrown in while
        #   executing the module definition.
        return self.phase is phase_finding_structure or self.phase is phase_collecting_components

    def compiled_module(self):
        assert(self.phase is phase_compiled)
        return CompiledModule(self.name, self.components, self.code, self.thin_instances)

    def print_traces(self):
        for branches in self.traces:
            print("".join(["T" if branch else "F" for branch in branches]) + ":")
            for node in self.traces[branches]:
                print("  %s" % node)

    def start_execution(self):
        assert(self.phase is phase_none)
        self.phase = phase_finding_structure
        #print("START EXECUTION")
        self.cur_branch_idx = 0
        self.cur_trace = []
        self.cur_withstack = []
    def end_execution(self):
        assert(self.phase is phase_finding_structure)
        self.phase = phase_none
        #print("END EXECUTION")
        self.traces[tuple(self.cur_branches)] = self.cur_trace
        while len(self.cur_branches) > 0:
            if self.cur_branches[-1]:
                self.cur_branches[-1] = False
                break
            else:
                self.cur_branches = self.cur_branches[:-1]
    def keep_executing(self):
        return len(self.cur_branches) != 0

    def handle_assign_cur(self, dst, src):
        self.handle_assign(AssignCurNode(dst, src))
    def handle_assign_next(self, dst, src):
        self.handle_assign(AssignNextNode(dst, src))
    def handle_assign(self, node):
        if self.phase == phase_finding_structure:
            self.add_to_trace(node)
        elif self.phase != phase_collecting_components:
            raise Exception("Can only handle symbolic assignment while finding structure or collecting components!")
    def handle_boolify(self, val):
        if self.phase == phase_finding_structure:
            node = BoolifyNode(val)
            self.add_to_trace(node)
            if self.cur_branch_idx >= len(self.cur_branches):
                self.cur_branches.append(True)
            branch = self.cur_branches[self.cur_branch_idx]
            self.cur_branch_idx += 1
            return branch
        elif self.phase == phase_collecting_components:
            # we just need to return _some_ value. doesn't matter which,
            # since all component definitions should be at the top level.
            # TODO: is this actually true?
            # maybe we should have component definitions reified just like AssignCurNode; something like
            #   an AddComponent node which is a trace.
            return True
        elif self.phase != phase_collecting_components:
            raise Exception("Can only handle symbolic bool(...) conversion while finding structure or collecting components!")
    def handle_enter(self, cond):
        if self.phase == phase_finding_structure:
            #print("enter: %s" % cond)
            self.cur_withstack.append(WithNode(cond, []))
        elif self.phase != phase_collecting_components:
            raise Exception("Can only handle symbolic with (...) while finding structure or collecting components!")
    def handle_exit(self, cond):
        if self.phase == phase_finding_structure:
            #print("exit")
            self.cur_withstack, cur_with = self.cur_withstack[:-1], self.cur_withstack[-1]
            self.add_to_trace(cur_with)
        elif self.phase != phase_collecting_components:
            raise Exception("Can only handle symbolic with (...) while finding structure or collecting components!")
    def add_to_trace(self, basic_node):
        if self.phase == phase_finding_structure:
            if len(self.cur_withstack) > 0:
                self.cur_withstack[-1].body.append(basic_node)
            else:
                self.cur_trace.append(basic_node)
            #print("add: %s" % basic_node)
            #print("new trace: [%s]" % ", ".join(str(node) for node in self.cur_trace))
        elif self.phase != phase_collecting_components:
            raise Exception("Can only add symbolic operations to trace while finding structure or collecting components!")

    def start_collect_components(self):
        #print("submodules of module %s:" % self.name)
        #for submod in self.submodules:
        #    print("  %s", submod.name)
        assert(self.phase is phase_none)
        self.phase = phase_collecting_components
    def end_collect_components(self):
        assert(self.phase is phase_collecting_components)
        self.phase = phase_collected_components

    def add_input(self, vtype, name):
        if self.phase == phase_collecting_components:
            if name in self.components:
                raise Exception("Component '%s' is multiply defined." % name)
            self.components[name] = InputComponent(name, vtype)
    def add_wire(self, vtype, name):
        if self.phase == phase_collecting_components:
            if name in self.components:
                raise Exception("Component '%s' is multiply defined." % name)
            self.components[name] = WireComponent(name, vtype)
    def add_register(self, vtype, reset, name):
        if self.phase == phase_collecting_components:
            if name in self.components:
                raise Exception("Component '%s' is multiply defined." % name)
            self.components[name] = RegisterComponent(name, vtype, reset)
    def add_output(self, vtype, name):
        if self.phase == phase_collecting_components:
            if name in self.components:
                raise Exception("Component '%s' is multiply defined." % name)
            self.components[name] = OutputComponent(name, vtype)
    def add_instance(self, module, name):
        assert(isinstance(module, CompiledModule))
        if self.phase == phase_collecting_components:
            if name in self.thin_instances:
                raise Exception("Instance '%s' is multiply defined." % name)
            self.thin_instances[name] = ThinModuleInstance(name, module)

    def compile(self):
        assert(self.phase is phase_collected_components)
        self.code = self.compile_traces()
        self.phase = phase_compiled
        self.cur_branches = None
        self.cur_branch_idx = None
        self.cur_trace = None
        self.cur_withstack = None
        self.traces = None

    # assumes that in the module body, any `if` depending on a module node has it syntactically as the condition,
    # i.e. there is no code like:
    #        cond = bool(node)
    #        ...
    #        if cond:
    #            ...
    # and rather this would just be
    #        if node:
    #            ...
    def compile_traces(self):
        # take the linear traces and turn them into a tree representation of the module's code

        # find a common prefix and suffix of a collection of lists.
        def find_prefix_suffix(lists):
            lists = [list(lst) for lst in lists]
            prefix = []
            while len(lists[0]) > 0:
                node = lists[0][0]
                in_prefix = all(len(lst) > 0 and lst[0] == node for lst in lists)
                if in_prefix:
                    prefix.append(node)
                    for i in range(len(lists)):
                        lists[i] = lists[i][1:]
                else:
                    break
            suffix = []
            while len(lists[0]) > 0:
                node = lists[0][-1]
                in_suffix = all(len(lst) > 0 and lst[-1] == node for lst in lists)
                if in_suffix:
                    suffix.insert(0, node)
                    for i in range(len(lists)):
                        lists[i] = lists[i][:-1]
                else:
                    break
            return prefix, suffix

        # take a subset of traces corresponding to a block of code
        # and return a single code block for it
        def compile_trace_set(traces):
            #print("COMPILING TRACE SET:")
            #for branches in self.traces:
            #    print("".join(["T" if branch else "F" for branch in branches]) + ":",
            #            "; ".join([str(node) for node in self.traces[branches]]))
            if len(traces) == 1:
                return traces[()]
            else:
                prefix, suffix = find_prefix_suffix(traces.values())
                #print("prefix:", "[" + ", ".join([str(node) for node in prefix]) + "]")
                #print("suffix:", "[" + ", ".join([str(node) for node in suffix]) + "]")
                for branches in traces:
                    trace = traces[branches]
                    if suffix:
                        trace = trace[len(prefix):-len(suffix)]
                    else:
                        trace = trace[len(prefix):]
                    traces[branches] = trace
                # prefix should have a `bool(...)` node in it.
                # we assume the last such node is the first branch condition.
                cond = None
                for node in reversed(prefix):
                    if isinstance(node, BoolifyNode):
                        cond = node.val
                        break
                if cond is None:
                    raise Exception("Common prefix in trace set did not have any boolify!")
                true_traces, false_traces = {}, {}
                for branches in traces:
                    trace = traces[branches]
                    if branches[0] == True:
                        true_traces[branches[1:]] = trace
                    else:
                        false_traces[branches[1:]] = trace
                true_branch = compile_trace_set(true_traces)
                false_branch = compile_trace_set(false_traces)
                prefix = [node for node in prefix if not isinstance(node, BoolifyNode)]
                suffix = [node for node in suffix if not isinstance(node, BoolifyNode)]
                return prefix + [IfNode(cond, true_branch, false_branch)] + suffix

        return compile_trace_set(self.traces)
