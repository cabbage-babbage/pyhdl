from modulestack import top_module, exists_top_module
from valuetype import UnknownType

# TODO: figure out how to remove all these __hash__ functions;
# it seems kind of silly

# is computation currently symbolic? it is symbolic
# if we're currently executing a module's body to find its code structure.
def is_symbolic():
    return exists_top_module() and top_module().is_symbolic()
# TODO: remove this disgusting hack
equality_version = None
def set_structural_equality():
    global equality_version
    equality_version = "structural"
def set_identity_equality():
    global equality_version
    equality_version = "identity"
def is_structural():
    return equality_version is "structural"
def is_identity():
    return equality_version is "identity"

class CodeNode:
    def copy(self):
        print(self)
        raise Exception("Implement in subclass.")
class IfNode(CodeNode):
    def __init__(self, cond, true_branch, false_branch):
        self.cond = cond
        self.true_branch = true_branch
        self.false_branch = false_branch
    def copy(self):
        return IfNode(self.cond.copy(),
                [node.copy() for node in self.true_branch], 
                [node.copy() for node in self.false_branch])
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, IfNode) and other.cond == self.cond and other.true_branch == self.true_branch and other.false_branch == self.false_branch
    def __str__(self):
        s = "if %s: [%s]" % (self.cond, "; ".join([str(node) for node in self.true_branch]))
        if self.false_branch:
            s += " else: [%s]" % "; ".join([str(node) for node in self.false_branch])
        return s
class WithNode(CodeNode):
    def __init__(self, cond, body):
        self.cond = cond
        self.body = body
    def copy(self):
        return WithNode(self.cond.copy(),
                [node.copy() for node in self.body])
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, WithNode) and other.cond == self.cond and other.body == self.body
    def __str__(self):
        return "with %s: [%s]" % (self.cond, "; ".join([str(node) for node in self.body]))
class BoolifyNode(CodeNode):
    def __init__(self, val):
        self.val = val
    def copy(self):
        return BoolifyNode(val.copy())
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, BoolifyNode) and other.val == self.val
    def __str__(self):
        return "bool(%s)" % self.val
class AssignCurNode(CodeNode):
    def __init__(self, dst, src):
        self.dst = dst
        self.src = src
    def copy(self):
        return AssignCurNode(self.dst.copy(), self.src.copy())
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, AssignCurNode) and other.dst == self.dst and other.src == self.src
    def __str__(self):
        return "%s.cur = %s" % (self.dst, self.src)
class AssignNextNode(CodeNode):
    def __init__(self, dst, src):
        self.dst = dst
        self.src = src
    def copy(self):
        return AssignNextNode(self.dst.copy(), self.src.copy())
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, AssignNextNode) and other.dst == self.dst and other.src == self.src
    def __str__(self):
        return "%s.next = %s" % (self.dst, self.src)

class ExprNode(CodeNode):
    def __init__(self, vtype=None):
        self.vtype = vtype if vtype is not None else UnknownType()

    def __setattr__(self, name, value):
        if is_symbolic():
            if name == "cur":
                top_module().handle_assign_cur(self, value)
            elif name == "next":
                top_module().handle_assign_next(self, value)
        #else:
        super().__setattr__(name, value)

    def __bool__(self):
        if is_symbolic():
            return top_module().handle_boolify(self)
        else:
            return super().__bool__()
    def __enter__(self):
        if is_symbolic():
            top_module().handle_enter(self)
        else:
            return super().__enter__()
    def __exit__(self, exc_type, exc_value, traceback):
        if is_symbolic():
            top_module().handle_exit(self)
        else:
            return super().__exit__(exc_type, exc_value, traceback)

    def __lt__(self, right):
        if is_symbolic():
            return LtNode(self, right)
        else:
            return super().__lt__(right)
    def __le__(self, right):
        if is_symbolic():
            return LeNode(self, right)
        else:
            return super().__le__(right)
    def __eq__(self, right):
        if is_symbolic():
            return EqNode(self, right)
        elif is_identity():
            return self is other
        else:
            return super().__eq__(right)
    def __ne__(self, right):
        if is_symbolic():
            return NeNode(self, right)
        elif is_identity():
            return self is not other
        else:
            return super().__ne__(right)
    def __gt__(self, right):
        if is_symbolic():
            return GtNode(self, right)
        else:
            return super().__gt__(right)
    def __ge__(self, right):
        if is_symbolic():
            return GeNode(self, right)
        else:
            return super().__ge__(right)
    def __add__(self, right):
        if is_symbolic():
            return AddNode(self, right)
        else:
            return super().__add__(right)
    def __sub__(self, right):
        if is_symbolic():
            return SubNode(self, right)
        else:
            return super().__sub__(right)
    def __mul__(self, right):
        if is_symbolic():
            return MulNode(self, right)
        else:
            return super().__mul__(right)
    def __floordiv__(self, right):
        if is_symbolic():
            return FloorDivNode(self, right)
        else:
            return super().__floordiv__(right)
    def __mod__(self, right):
        if is_symbolic():
            return ModNode(self, right)
        else:
            return super().__mod__(right)
    def __lshift__(self, right):
        if is_symbolic():
            return LShiftNode(self, right)
        else:
            return super().__lshift__(right)
    def __rshift__(self, right):
        if is_symbolic():
            return RShiftNode(self, right)
        else:
            return super().__rshift__(right)
    def __and__(self, right):
        if is_symbolic():
            return AndNode(self, right)
        else:
            return super().__and__(right)
    def __xor__(self, right):
        if is_symbolic():
            return XorNode(self, right)
        else:
            return super().__xor__(right)
    def __or__(self, right):
        if is_symbolic():
            return OrNode(self, right)
        else:
            return super().__or__(right)
    def __neg__(self):
        if is_symbolic():
            return NegNode(self)
        else:
            return super().__neg__()
    def __pos__(self):
        if is_symbolic():
            return PosNode(self)
        else:
            return super().__pos__()
    def __invert__(self):
        if is_symbolic():
            return InvertNode(self)
        else:
            return super().__invert__()
    def __getitem__(self, index):
        if is_symbolic():
            return IndexNode(self, index)
        else:
            return super().__getitem__(index)
    def __getattr__(self, index):
        if is_symbolic():
            return IndexNode(self, index)
        else:
            return super().__getattr__(index)

class InputNode(ExprNode):
    def __init__(self, vtype, name):
        super().__init__(vtype)
        self.name = name
    def copy(self):
        return InputNode(self.vtype.copy(), self.name)
    def __hash__(self):
        return hash(self.name)
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, InputNode) and other.name == self.name and other.vtype == self.vtype
    def __str__(self):
        return self.name
class WireNode(ExprNode):
    def __init__(self, vtype, name):
        super().__init__(vtype)
        self.name = name
    def copy(self):
        return WireNode(self.vtype.copy(), self.name)
    def __hash__(self):
        return hash(self.name)
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, WireNode) and other.name == self.name and other.vtype == self.vtype
    def __str__(self):
        return self.name
class RegisterNode(ExprNode):
    def __init__(self, vtype, name):
        super().__init__(vtype)
        self.name = name
    def copy(self):
        return RegisterNode(self.vtype.copy(), self.name)
    def __hash__(self):
        return hash(self.name)
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, RegisterNode) and other.name == self.name and other.vtype == self.vtype
    def __str__(self):
        return self.name
class OutputNode(ExprNode):
    def __init__(self, vtype, name):
        super().__init__(vtype)
        self.name = name
    def copy(self):
        return OutputNode(self.vtype.copy(), self.name)
    def __hash__(self):
        return hash(self.name)
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, OutputNode) and other.name == self.name and other.vtype == self.vtype
    def __str__(self):
        return self.name

# this should never show up in .code!
class InstanceNode:
    def __init__(self, module, inst_name):
        self.module = module
        self.inst_name = inst_name
        self.io = InstanceNodeIO(module, inst_name)
class InstanceNodeIO:
    def __init__(self, module, inst_name):
        #assert(isinstance(module, CompiledModule))
        self.__reserved_module__ = module
        self.__reserved_inst_name__ = inst_name
    def get_component(self, name):
        module = self.__reserved_module__
        inst_name = self.__reserved_inst_name__
        if name in module.components:
            component = module.components[name]
            if component.is_interface:
                # TODO: somehow verify that the module using the current
                # module actually owns this module's module as a submodule.
                return InstanceComponentNode(inst_name, name)
            else:
                raise IndexError("'%s' is not an interface component of instance '%s' of module '%s'." % (name, inst_name, module.name))
        else:
            raise IndexError("Interface component '%s' does not exist in instance '%s' of module '%s'." % (name, inst_name, module.name))
    def __getitem__(self, name):
        return self.get_component(name)
    def __getattr__(self, name):
        return self.get_component(name)

class InstanceComponentNode(ExprNode):
    def __init__(self, instance_name, component_name, vtype=None):
        assert(isinstance(instance_name, str))
        assert(isinstance(component_name, str))
        super().__init__(vtype)
        self.instance_name = instance_name
        self.component_name = component_name
    def copy(self):
        return InstanceComponentNode(self.instance_name, self.component_name, self.vtype.copy())
    def __hash__(self):
        return hash((self.instance_name, self.component_name))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, InstanceComponentNode) and other.instance_name == self.instance_name and other.component_name == self.component_name
    def __str__(self):
        return "%s.io.%s" % (self.instance_name, self.component_name)

class LiteralNode(ExprNode):
    def __init__(self, vtype):
        super().__init__(vtype)
class DefaultValueNode(LiteralNode):
    def __init__(self, vtype):
        super().__init__(vtype)
    def copy(self):
        return DefaultValueNode(self.vtype.copy())
    def __hash__(self):
        return hash(str(self.vtype))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, DefaultValueNode) and other.vtype == self.vtype
    def __str__(self):
        return "default<%s>" % self.vtype
class LiteralUIntNode(ExprNode):
    def __init__(self, width, value, vtype=None):
        super().__init__(vtype)
        self.width = width
        self.value = value
    def copy(self):
        return LiteralUIntNode(self.width, self.value, self.vtype.copy())
    def __hash__(self):
        return hash(self.value)
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, LiteralUIntNode) and other.width == self.width and other.value == self.value
    def __str__(self):
        width = self.width if isinstance(self.vtype, UnknownType) else self.vtype.width
        return "UInt<%s%s>(%d)" % (width, "." if self.vtype is None else "", self.value)

class ComputationExprNode(ExprNode):
    def __init__(self, vtype):
        super().__init__(vtype)
class EqNode(ComputationExprNode):
    def __init__(self, left, right, vtype=None):
        super().__init__(vtype)
        self.left = left
        self.right = right
    def copy(self):
        return EqNode(self.left.copy(), self.right.copy(), self.vtype.copy())
    def __hash__(self):
        return hash((self.left, self.right))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, EqNode) and other.left == self.left and other.right == self.right
    def __str__(self):
        return "(%s == %s)" % (self.left, self.right)
class CatNode(ComputationExprNode):
    def __init__(self, left, right, vtype=None):
        super().__init__(vtype)
        self.left = left
        self.right = right
    def copy(self):
        return CatNode(self.left.copy(), self.right.copy(), self.vtype.copy())
    def __hash__(self):
        return hash((self.left, self.right))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, CatNode) and other.left == self.left and other.right == self.right
    def __str__(self):
        return "cat(%s, %s)" % (self.left, self.right)
class MuxNode(ComputationExprNode):
    def __init__(self, sel, inputs, vtype=None):
        super().__init__(vtype)
        self.sel = sel
        self.inputs = inputs
    def copy(self):
        return MuxNode(self.sel.copy(), [inp.copy() for inp in self.inputs], self.vtype.copy())
    def __hash__(self):
        return hash(tuple([self.sel] + self.inputs))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, MuxNode) and other.sel == self.sel and list_eq(other.inputs, self.inputs)
    def __str__(self):
        return "mux(%s, %s)" % (self.sel, ", ".join(str(inp) for inp in self.inputs))
class SubNode(ComputationExprNode):
    def __init__(self, left, right, vtype=None):
        super().__init__(vtype)
        self.left = left
        self.right = right
    def copy(self):
        return SubNode(self.left.copy(), self.right.copy(), self.vtype.copy())
    def __hash__(self):
        return hash((self.left, self.right))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, SubNode) and other.left == self.left and other.right == self.right
    def __str__(self):
        return "(%s - %s)" % (self.left, self.right)
class AddNode(ComputationExprNode):
    def __init__(self, left, right, vtype=None):
        super().__init__(vtype)
        self.left = left
        self.right = right
    def copy(self):
        return AddNode(self.left.copy(), self.right.copy(), self.vtype.copy())
    def __hash__(self):
        return hash((self.left, self.right))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, AddNode) and other.left == self.left and other.right == self.right
    def __str__(self):
        return "(%s + %s)" % (self.left, self.right)
class LtNode(ComputationExprNode):
    def __init__(self, left, right, vtype=None):
        super().__init__(vtype)
        self.left = left
        self.right = right
    def copy(self):
        return LtNode(self.left.copy(), self.right.copy(), self.vtype.copy())
    def __hash__(self):
        return hash((self.left, self.right))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, LtNode) and other.left == self.left and other.right == self.right
    def __str__(self):
        return "(%s < %s)" % (self.left, self.right)
class LShiftNode(ComputationExprNode):
    def __init__(self, left, right, vtype=None):
        super().__init__(vtype)
        self.left = left
        self.right = right
    def copy(self):
        return LShiftNode(self.left.copy(), self.right.copy(), self.vtype.copy())
    def __hash__(self):
        return hash((self.left, self.right))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, LShiftNode) and other.left == self.left and other.right == self.right
    def __str__(self):
        return "(%s << %s)" % (self.left, self.right)
class RShiftNode(ComputationExprNode):
    def __init__(self, left, right, vtype=None):
        super().__init__(vtype)
        self.left = left
        self.right = right
    def copy(self):
        return RShiftNode(self.left.copy(), self.right.copy(), self.vtype.copy())
    def __hash__(self):
        return hash((self.left, self.right))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, RShiftNode) and other.left == self.left and other.right == self.right
    def __str__(self):
        return "(%s >> %s)" % (self.left, self.right)
class AndNode(ComputationExprNode):
    def __init__(self, left, right, vtype=None):
        super().__init__(vtype)
        self.left = left
        self.right = right
    def copy(self):
        return AndNode(self.left.copy(), self.right.copy(), self.vtype.copy())
    def __hash__(self):
        return hash((self.left, self.right))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, AndNode) and other.left == self.left and other.right == self.right
    def __str__(self):
        return "(%s & %s)" % (self.left, self.right)
class OrNode(ComputationExprNode):
    def __init__(self, left, right, vtype=None):
        super().__init__(vtype)
        self.left = left
        self.right = right
    def copy(self):
        return OrNode(self.left.copy(), self.right.copy(), self.vtype.copy())
    def __hash__(self):
        return hash((self.left, self.right))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, OrNode) and other.left == self.left and other.right == self.right
    def __str__(self):
        return "(%s | %s)" % (self.left, self.right)
class InvertNode(ComputationExprNode):
    def __init__(self, right, vtype=None):
        super().__init__(vtype)
        self.right = right
    def copy(self):
        return InvertNode(self.right.copy(), self.vtype.copy())
    def __hash__(self):
        return hash(self.right)
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, InvertNode) and other.right == self.right
    def __str__(self):
        return "~%s" % self.right
class IndexNode(ExprNode):
    def __init__(self, aggregate, index, vtype=None):
        super().__init__(vtype)
        self.aggregate = aggregate
        self.index = index
    def copy(self):
        return IndexNode(self.aggregate.copy(), self.index.copy() if isinstance(self.index, ExprNode) else self.index, self.vtype.copy())
    def __hash__(self):
        if isinstance(self.index, slice):
            return hash((self.aggregate, self.index.start, self.index.stop, self.index.step))
        else:
            return hash((self.aggregate, self.index))
    def __eq__(self, other):
        if is_symbolic():
            return super().__eq__(other)
        elif is_identity():
            return self is other
        else:
            return isinstance(other, IndexNode) and other.aggregate == self.aggregate and other.index == self.index
    def __str__(self):
        index_str = ""
        if isinstance(self.index, int):
            index_str = str(self.index)
        elif isinstance(self.index, str):
            return "%s.%s" % (self.aggregate, self.index)
        elif isinstance(self.index, slice):
            if self.index.start is not None:
                index_str += str(self.index.start)
            index_str += ":"
            if self.index.stop is not None:
                index_str += str(self.index.stop)
            if self.index.step is not None:
                index_str += ":"
                index_str += str(self.index.step)
        else:
            index_str = str(self.index)
        return "%s[%s]" % (self.aggregate, index_str)
