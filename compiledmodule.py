from codenode import *
from valuetype import *

class ModuleComponent:
    def __init__(self, name, vtype):
        self.name = name
        self.vtype = vtype
    def __str__(self):
        return self.name
class InputComponent(ModuleComponent):
    def __init__(self, name, vtype):
        super().__init__(name, vtype)
        self.is_interface = True
    def copy(self):
        return InputComponent(self.name, self.vtype.copy())
class WireComponent(ModuleComponent):
    def __init__(self, name, vtype):
        super().__init__(name, vtype)
        self.is_interface = False
    def copy(self):
        return WireComponent(self.name, self.vtype.copy())
class RegisterComponent(ModuleComponent):
    def __init__(self, name, vtype, reset):
        super().__init__(name, vtype)
        self.reset = reset
        self.is_interface = False
    def copy(self):
        return RegisterComponent(self.name, self.vtype.copy(), self.reset.copy())
class OutputComponent(ModuleComponent):
    def __init__(self, name, vtype):
        super().__init__(name, vtype)
        self.is_interface = True
    def copy(self):
        return OutputComponent(self.name, self.vtype.copy())

# a "thin" instance of a module: just an instance-specific name,
# and a pointer to the CompiledModule specifying the instance's behavior.
class ThinModuleInstance:
    def __init__(self, name, module):
        assert(isinstance(module, CompiledModule))
        self.name = name
        self.module = module

    def as_full_instance(self):
        assert(self.module.typechecked)
        copied_components = { name: self.module.components[name].copy() for name in self.module.components }
        copied_code = [node.copy() for node in self.module.code]
        full_instances = { name: self.module.thin_instances[name].as_full_instance() for name in self.module.thin_instances }
        return ModuleInstance(self.module.name, self.name, copied_components, copied_code, full_instances)

# a "full" instance of a module: it fully owns (i.e. has a deep copy of) all subinstances,
# and such a module instance should be synthesizable/simulatable
class ModuleInstance:
    def __init__(self, module_name, name, components, code, instances):
        self.module_name = module_name # not strictly necessary, but may be nice for debugging/pprinting
        self.name = name
        self.components = components
        self.code = code
        self.instances = instances

        for name in self.instances:
            instance = self.instances[name]
            assert(isinstance(instance, ModuleInstance))

        self.widths_inferred = False
        self.verified_no_cycles = False

    def infer_widths(self):
        set_identity_equality()
        # constraints are of the form: [node, f, arg1, ..., argn]
        #   where each arg is either a vtype instance or an int
        #   and f is in ["==", "<-", ">=", "sum", "max", "max+1"]
        # this returns a collection of constraints that were found,
        # along with a list of nodes whose types must be inferred.
        # (return value is (constraints, nodes).)
        def constrain_widths_in_instance(instance):
            constraints = []
            nodes = list(instance.components.values()) # make sure we will infer all component types

            def constrain_assign(node, dst_vtype, src_vtype):
                if isinstance(dst_vtype, UInt) or isinstance(dst_vtype, SInt):
                    constraints.append((node, ">=", dst_vtype, src_vtype))
                elif isinstance(dst_vtype, Vec):
                    constraints.append((node, "==", dst_vtype, src_vtype))
                    constrain_assign(node, dst_vtype.etype, src_vtype.etype)
                elif isinstance(dst_vtype, Struct):
                    for name in dst_vtype.elems:
                        constrain_assign(node, dst_vtype.elems[name], src_vtype.elems[name])
                else:
                    raise NotImplemented

            def constrain_eq(node, dst_vtype, src_vtype):
                if isinstance(dst_vtype, UInt) or isinstance(dst_vtype, SInt):
                    constraints.append((node, "==", dst_vtype, src_vtype))
                elif isinstance(dst_vtype, Vec):
                    constraints.append((node, "==", dst_vtype, src_vtype))
                    constrain_eq(node, dst_vtype.etype, src_vtype.etype)
                elif isinstance(dst_vtype, Struct):
                    for name in dst_vtype.elems:
                        constrain_eq(node, dst_vtype.elems[name], src_vtype.elems[name])
                else:
                    raise NotImplemented

            def constrain_unidirectional_eq(node, dst_vtype, src_vtype):
                if isinstance(dst_vtype, UInt) or isinstance(dst_vtype, SInt):
                    constraints.append((node, "<-", dst_vtype, src_vtype))
                elif isinstance(dst_vtype, Vec):
                    constraints.append((node, "<-", dst_vtype, src_vtype))
                    constrain_unidirectional_eq(node, dst_vtype.etype, src_vtype.etype)
                elif isinstance(dst_vtype, Struct):
                    for name in dst_vtype.elems:
                        constrain_unidirectional_eq(node, dst_vtype.elems[name], src_vtype.elems[name])
                else:
                    raise NotImplemented

            def bits_of_num(n):
                bits = 0
                while n > 0:
                    n = n // 2
                    bits += 1
                return bits

            def constrain_node(node, container_instance):
                nodes.append(node)

                if isinstance(node, InputNode) or isinstance(node, WireNode) or isinstance(node, RegisterNode) or isinstance(node, OutputNode):
                    constrain_eq(node, node.vtype, container_instance.components[node.name].vtype)
                elif isinstance(node, InstanceComponentNode):
                    instance = container_instance.instances[node.instance_name]
                    component = instance.components[node.component_name]
                    constrain_eq(node, node.vtype, component.vtype)
                elif isinstance(node, DefaultValueNode):
                    pass
                elif isinstance(node, LiteralUIntNode):
                    bits_required = bits_of_num(node.value)
                    constraints.append((node, ">=", node.vtype, bits_required))
                elif isinstance(node, EqNode):
                    if isinstance(node.vtype, UInt):
                        constraints.append((node, "==", node.vtype, 1))
                        # TODO: no constraints on left/right? or require equality?
                        #constraints.append((node, "==", node.left.vtype, node.right.vtype))
                    else:
                        print(node)
                        raise NotImplemented
                    constrain_node(node.left, container_instance)
                    constrain_node(node.right, container_instance)
                elif isinstance(node, CatNode):
                    if isinstance(node.vtype, UInt):
                        constraints.append((node, "sum", node.vtype, node.left.vtype, node.right.vtype))
                    elif isinstance(node.vtype, Vec):
                        constraints.append((node, "sum", node.vtype, node.left.vtype, node.right.vtype))
                        constrain_eq(node, node.left.vtype, node.right.vtype)
                    else:
                        print(node)
                        raise NotImplemented
                    constrain_node(node.left, container_instance)
                    constrain_node(node.right, container_instance)
                elif isinstance(node, MuxNode):
                    # TODO: constrain number of inputs to be a power of 2?
                    sel_bits_required = bits_of_num(len(node.inputs) - 1)
                    constraints.append((node.sel, ">=", node.sel.vtype, sel_bits_required))
                    for i in range(len(node.inputs) - 1):
                        constrain_eq(node, node.inputs[i].vtype, node.inputs[i+1].vtype)
                    constrain_eq(node, node.vtype, node.inputs[0].vtype)
                    constrain_node(node.sel, container_instance)
                    for inp in node.inputs:
                        constrain_node(inp, container_instance)
                elif isinstance(node, SubNode):
                    if isinstance(node.vtype, UInt):
                        # TODO: should width(a - b) be width(a), or max(width(a), width(b))?
                        constraints.append((node, "==", node.vtype, node.left.vtype))
                    else:
                        print(node)
                        raise NotImplemented
                    constrain_node(node.left, container_instance)
                    constrain_node(node.right, container_instance)
                elif isinstance(node, AddNode):
                    if isinstance(node.vtype, UInt):
                        # this is not a full add; it zero-extends the smaller one and can overflow
                        constraints.append((node, "max", node.vtype, node.left.vtype, node.right.vtype))
                    else:
                        print(node)
                        raise NotImplemented
                    constrain_node(node.left, container_instance)
                    constrain_node(node.right, container_instance)
                elif isinstance(node, LtNode):
                    if isinstance(node.vtype, UInt):
                        constraints.append((node, "==", node.vtype, 1))
                        # TODO: constrain left/right at all?
                    else:
                        print(node)
                        raise NotImplemented
                    constrain_node(node.left, container_instance)
                    constrain_node(node.right, container_instance)
                elif isinstance(node, LShiftNode):
                    if isinstance(node.vtype, UInt):
                        # this is not a full <<; it will result in 0 if shift amount is too large.
                        constraints.append((node, "==", node.vtype, node.left.vtype))
                    else:
                        print(node)
                        raise NotImplemented
                    constrain_node(node.left, container_instance)
                    constrain_node(node.right, container_instance)
                elif isinstance(node, RShiftNode):
                    if isinstance(node.vtype, UInt):
                        constraints.append((node, "==", node.vtype, node.left.vtype))
                    else:
                        print(node)
                        raise NotImplemented
                    constrain_node(node.left, container_instance)
                    constrain_node(node.right, container_instance)
                elif isinstance(node, AndNode):
                    if isinstance(node.vtype, UInt):
                        # bitwise and: need same number of bits
                        constraints.append((node, "==", node.vtype, node.left.vtype))
                        constraints.append((node, "==", node.vtype, node.right.vtype))
                        constraints.append((node, "==", node.left.vtype, node.right.vtype))
                    else:
                        print(node)
                        raise NotImplemented
                    constrain_node(node.left, container_instance)
                    constrain_node(node.right, container_instance)
                elif isinstance(node, OrNode):
                    if isinstance(node.vtype, UInt):
                        # bitwise or: need same number of bits
                        constraints.append((node, "==", node.vtype, node.left.vtype))
                        constraints.append((node, "==", node.vtype, node.right.vtype))
                        constraints.append((node, "==", node.left.vtype, node.right.vtype))
                    else:
                        print(node)
                        raise NotImplemented
                    constrain_node(node.left, container_instance)
                    constrain_node(node.right, container_instance)
                elif isinstance(node, InvertNode):
                    if isinstance(node.vtype, UInt):
                        constraints.append((node, "==", node.vtype, node.right.vtype))
                    else:
                        print(node)
                        raise NotImplemented
                    constrain_node(node.right, container_instance)
                elif isinstance(node, IndexNode):
                    # index is str, int, codenode, or slice(int/None, int/None, int/None)
                    constrain_node(node.aggregate, container_instance)
                    if isinstance(node.index, str):
                        if isinstance(node.aggregate.vtype, Struct):
                            constraints.append((node, "==", node.vtype, node.aggregate.vtype.elems[node.index]))
                        else:
                            print(node)
                            raise NotImplemented
                    elif isinstance(node.index, int):
                        # assert that the width is large enough so that the index points to a valid bit
                        if node.index >= 0:
                            constraints.append((node, ">=", node.aggregate.vtype, node.index + 1))
                        else:
                            constraints.append((node, ">=", node.aggregate.vtype, -node.index))

                        if isinstance(node.aggregate.vtype, UInt):
                            # assert that the result is a single bit
                            constraints.append((node, "==", node.vtype, 1))
                        elif isinstance(node.aggregate.vtype, Vec):
                            # assert that the result type is precisely the element type
                            constrain_eq(node, node.vtype, node.aggregate.vtype.etype)
                        else:
                            print(node)
                            raise NotImplemented
                    elif isinstance(node.index, ExprNode):
                        constrain_node(node.index, container_instance)
                        # TODO: how to constrain this? should it just be some kind of warning?
                        print("WARNING: skipping any constraints from expression '%s'" % node)
                    elif isinstance(node.index, slice):
                        start, stop, step = node.index.start, node.index.stop, node.index.step
                        required_len = None
                        if start is not None:
                            req = start + 1 if start >= 0 else -start
                            if required_len is None or required_len < req:
                                required_len = req
                        if stop is not None:
                            req = stop if stop >= 0 else -stop
                            if required_len is None or required_len < req:
                                required_len = req
                        if required_len is not None:
                            constraints.append((node, ">=", node.aggregate.vtype, required_len))

                        if isinstance(node.aggregate.vtype, UInt) or isinstance(node.aggregate.vtype, Vec):
                            # assert that the resulting aggregate has the right width
                            if (start is None or start >= 0) and (stop is None or stop >= 0) and (step is None):
                                if start is None and stop is None: # agg[:]
                                    # width(agg[:]) = width(agg)
                                    constraints.append((node, "==", node.vtype, node.aggregate.vtype))
                                elif start is not None and stop is None: # agg[start:]
                                    # width(agg[start:]) = width(agg) - start
                                    constraints.append((node, "sum", node.vtype, node.aggregate.vtype, -start))
                                elif start is None and stop is not None: # agg[:stop]
                                    # width(agg[:stop]) = stop
                                    constraints.append((node, "==", node.vtype, stop))
                                else: # agg[start:stop]
                                    if start <= stop:
                                        constraints.append((node, ">=", node.vtype, stop - start))
                                    else:
                                        # indices are the wrong way; zero-width
                                        constraints.append((node, ">=", node.vtype, 0))
                            elif (start is None) and (stop is not None and stop < 0) and (step is None):
                                # width(agg[:-n]) = width(agg) - n
                                constraints.append((node, "sum", node.vtype, node.aggregate.vtype, stop))
                            else:
                                # TODO: handle cases where start/stop are negative, or where step is given 
                                print(node)
                                raise NotImplemented

                            if isinstance(node.aggregate.vtype, Vec):
                                # also make sure that the resulting Vec has etype matching the original Vec
                                constrain_eq(node, node.vtype.etype, node.aggregate.vtype.etype)
                        else:
                            print(node)
                            raise NotImplemented
                    else:
                        print(node)
                        raise NotImplemented
                else:
                    print(node)
                    raise NotImplemented

            def constrain_widths_in_code(code, container_instance):
                for node in code:
                    if isinstance(node, IfNode):
                        constrain_node(node.cond, container_instance)
                        constraints.append((node.cond, "==", node.cond.vtype, 1))
                        constrain_widths_in_code(node.true_branch, container_instance)
                        constrain_widths_in_code(node.false_branch, container_instance)
                    elif isinstance(node, WithNode):
                        constrain_node(node.cond, container_instance)
                        constraints.append((node.cond, "==", node.cond.vtype, 1))
                        constrain_widths_in_code(node.body, container_instance)
                    elif isinstance(node, AssignCurNode):
                        constrain_assign(node, node.dst.vtype, node.src.vtype)
                        constrain_node(node.dst, container_instance)
                        constrain_node(node.src, container_instance)
                    elif isinstance(node, AssignNextNode):
                        constrain_assign(node, node.dst.vtype, node.src.vtype)
                        constrain_node(node.dst, container_instance)
                        constrain_node(node.src, container_instance)
                    else:
                        raise NotImplemented

            for component in instance.components.values():
                constrain_unidirectional_eq(component, component.vtype, component.vtype.copy())
            constrain_widths_in_code(instance.code, instance)
            for subinstance in instance.instances.values():
                subconstraints, subnodes = constrain_widths_in_instance(subinstance)
                constraints.extend(subconstraints)
                nodes.extend(subnodes)
            
            return constraints, nodes

        # constraints are of the form: [node, P, arg1, ..., argn]
        #   where each arg is either a vtype instance or an int
        #   and P is in ["==", "<-", ">=", "sum", "max", "max+1"]
        def solve_constraints(constraints, nodes):
            while True:
                #print("new iter")

                any_changes = False
                def set_width(c, vtype, new_width):
                    #print("setting width to %d (was %s), for '%s'" % (new_width, vtype.width, c[0]))
                    #print("   before: %s %s" % (c[2], c[1]), *c[3:])
                    vtype.width = new_width
                    #print("    after: %s %s" % (c[2], c[1]), *c[3:])
                    nonlocal any_changes
                    any_changes = True

                loose_cs = [] # constraints missing too many arguments to be able to infer a last one; useful for reporting later
                unsat_cs = [] # unsatisfied constraints
                for c in constraints:
                    node, P, args = c[0], c[1], c[2:]
                    if P == "==":
                        assert(len(args) == 2)
                        i, j = args
                        if isinstance(j, int):
                            if i.width is not Q: # % = j
                                if i.width != j:
                                    unsat_cs.append(c)
                                    if i.width < j:
                                        set_width(c, i, j)
                            else: # * = j
                                set_width(c, i, j)
                        else:
                            if i.width is not Q and j.width is not Q: # % = %
                                if i.width != j.width:
                                    unsat_cs.append(c)
                                    if i.width < j.width:
                                        set_width(c, i, j.width)
                                    elif i.width > j.width:
                                        set_width(c, j, i.width)
                            elif i.width is Q and j.width is not Q: # * = %
                                set_width(c, i, j.width)
                            elif i.width is not Q and j.width is Q: # % = *
                                set_width(c, j, i.width)
                            else:
                                loose_cs.append(c)
                    elif P == "<-":
                        # only assigns from right to left, and also does not care
                        # if the RHS.width is *
                        assert(len(args) == 2)
                        i, j = args
                        if i.width is not Q and j.width is not Q: # % = %
                            if i.width != j.width:
                                unsat_cs.append(c)
                                if i.width < j.width:
                                    set_width(c, i, j.width)
                        elif i.width is Q and j.width is not Q: # * = %
                            set_width(c, i, j.width)
                    elif P == "sum":
                        assert(len(args) == 3)
                        i, j, k = args
                        if isinstance(k, int):
                            if i.width is not Q and j.width is not Q: # % = % + k
                                if i.width != j.width + k:
                                    unsat_cs.append(c)
                            elif i.width is Q and j.width is not Q: # * = % + k
                                set_width(c, i, j.width + k)
                            elif i.width is not Q and j.width is Q: # % = * + k
                                if i.width >= k.width:
                                    set_width(c, j, i.width - k)
                                else:
                                    unsat_cs.append(c)
                            else:
                                loose_cs.append(c)
                        else:
                            if i.width is not Q and j.width is not Q and k.width is not Q: # % = % + %
                                if i.width != j.width + k.width:
                                    unsat_cs.append(c)
                            elif i.width is Q and j.width is not Q and k.width is not Q: # * = % + %
                                set_width(c, i, j.width + k.width)
                            elif i.width is not Q and j.width is Q and k.width is not Q: # % = * + %
                                if i.width >= k.width:
                                    set_width(c, j, i.width - k.width)
                                else:
                                    unsat_cs.append(c)
                            elif i.width is not Q and j.width is not Q and k.width is Q: # % = % + *
                                if i.width >= j.width:
                                    set_width(c, k, i.width - j.width)
                                else:
                                    unsat_cs.append(c)
                            else:
                                loose_cs.append(c)
                    elif P == "max":
                        assert(len(args) == 3)
                        i, j, k = args
                        if i.width is not Q and j.width is not Q and k.width is not Q: # % = max(%, %)
                            if i.width != max(j.width, k.width):
                                unsat_cs.append(c)
                        elif i.width is Q and j.width is not Q and k.width is not Q: # * = max(%, %)
                            set_width(c, i, max(j.width, k.width))
                        else:
                            loose_cs.append(c)
                    elif P == "=#":
                        assert(len(args) == 2)
                        i, val = args
                        if i.width is not Q: # % = val
                            if i.width != val:
                                unsat_cs.append(c)
                        else: # * = val
                            set_width(c, i, val)
                    elif P == ">=": # special behavior! "i.width >= j.width" can update i.width even if it is already set.
                        assert(len(args) == 2)
                        i, j = args
                        if isinstance(j, int):
                            if i.width is not Q: # % >= j
                                if i.width < j:
                                    set_width(c, i, j)
                            else: # * >= j
                                set_width(c, i, j)
                        else:
                            if i.width is not Q and j.width is not Q: # % >= %
                                if i.width < j.width:
                                    set_width(c, i, j.width)
                            elif i.width is Q and j.width is not Q: # * >= %
                                set_width(c, i, j.width)
                            else:
                                loose_cs.append(c)
                    else:
                        print(P)
                        raise NotImplemented
                if not any_changes:
                    # we're done!
                    if unsat_cs:
                        for c in unsat_cs:
                            print("Constraint from expression '%s' could not be satisfied." % c[0])
                            print_constraint(c, "  ")
                        return False # unsatisfiable
                    elif loose_cs or any(node.vtype.width is Q for node in nodes if isinstance(node.vtype, UInt) or isinstance(node.vtype, SInt) or isinstance(node.vtype, Vec)):
                        for c in loose_cs:
                            print("Constraint from expression '%s' does not have enough known values." % c[0])
                        printed_nodes = []
                        for node in nodes:
                            if isinstance(node.vtype, UInt) or isinstance(node.vtype, SInt) or isinstance(node.vtype, Vec):
                                if node.vtype.width is Q:
                                    already_printed = node in printed_nodes
                                    if not already_printed:
                                        print("Width of '%s' is unconstrained and cannot be inferred." % node)
                                        printed_nodes.append(node)
                        return False # underconstrained
                    else:
                        return True

        def print_constraint(c, prefix=""):
            print("%sfrom %s: %s %s" % (prefix, c[0], c[2], c[1]), *c[3:])
        constraints, nodes = constrain_widths_in_instance(self)
        #for c in constraints:
        #    print_constraint(c)
        #for node in nodes:
        #    print("node: %s" % node)

        satisfied = solve_constraints(constraints, nodes)
        if satisfied:
            self.widths_inferred = True
        else:
            raise TypeError("Width inference was unsuccessful.")

    # verify that there are no (possible) combinatorial cycles in the current instance or 
    # any of its subinstances.
    # examples of possible cycles:
    # *) w = Wire(...)
    #    w.cur = ~w
    # *) w1 = Wire(Vec(...))
    #    w2 = Wire(...)
    #    w1[index_expr].cur = w2
    #    w2.cur = w1[0]     <-- possible cycle if index_expr==0
    def verify_no_cycles(self):
        # require widths to be inferred so that we know how wide our vectors are
        assert(self.widths_inferred)

        set_identity_equality()
        class CombinatorialNode:
            def __init__(self, ref, assignment_required):
                self.ref = ref
                self.assignment_required = assignment_required
                self.deps = set()
                self.dep_of = set()
            def __hash__(self):
                return hash(id(self))
        class CombinatorialEdge:
            def __init__(self, ref, src, dst):
                self.ref = ref
                self.src = src
                self.dst = dst
                self.dst.deps.add(self.src)
                self.src.dep_of.add(self.dst)
        class Graph:
            def __init__(self):
                self.nodes = []
                self.edges = []
            # adds new node to the graph, and returns the node.
            # TODO: change 'assignment_required' -> 'intrinsically_assigned', or something like that
            def add_node(self, ref, assignment_required=True):
                node = CombinatorialNode(ref, assignment_required)
                self.nodes.append(node)
                return node
            def add_edge(self, ref, src, dst):
                # for sanity. TODO: can remove later for efficiency
                assert(src in self.nodes)
                assert(dst in self.nodes)
                self.edges.append(CombinatorialEdge(ref, src, dst))
            def add_edges(self, ref, srcs, dst):
                for src in srcs:
                    self.add_edge(ref, src, dst)

            def print(self):
                print("nodes:")
                for node in self.nodes:
                    print("  <%s>" % node.ref)
                print("edges:")
                for edge in self.edges:
                    print("  <%s> <= <%s>: from '%s'" % (edge.dst.ref, edge.src.ref, edge.ref))

        # these are refs; they are not combinatorial nodes.
        class VectorElement:
            def __init__(self, vec, width, index):
                assert(0 <= index <= width or -width <= index < 0)
                self.vec = vec
                self.width = width
                self.index = index + width if index < 0 else index
            def __eq__(self, other):
                return isinstance(other, VectorElement) and other.vec == self.vec and other.width == self.width and other.index == self.index
            def __hash__(self):
                return hash((self.vec, self.width, self.index))
            def __str__(self):
                return "%s[%s]" % (self.vec, self.index)
        class FieldElement:
            def __init__(self, struct, field_name):
                self.struct = struct
                self.field_name = field_name
            def __eq__(self, other):
                return isinstance(other, FieldElement) and other.struct == self.struct and other.field_name == self.field_name
            def __hash__(self):
                return hash((self.struct, self.field_name))
            def __str__(self):
                return "%s['%s']" % (self.struct, self.field_name)

        # 1) produce the combinatorial graph of the instance and all subinstances
        # 2) attempt to get topological sorting
        # 3) if there is a cycle, print it out!

        # these 'aggregate' combinatorial nodes will not be in the final combinatorial graph,
        # but they do organize the basic combinatorial nodes (which represent particular fields or vector elements)
        # so that we can properly set up the graph of basic nodes
        class VectorCombinatorialNode:
            def __init__(self, ref, width, elem_nodes):
                assert(len(elem_nodes) == width)
                self.ref = ref
                self.width = width
                self.elems = elem_nodes
            def elem_node(self, index):
                if not (0 <= index < self.width or -self.width <= index < 0):
                    raise IndexError(index)
                return self.elem[index]
        class StructCombinatorialNode:
            # fields is map from names to combinatorial-nodes (which can be Vector- or Struct-CombinatorialNode!)
            def __init__(self, ref, fields):
                self.ref = ref
                self.fields = fields
            def field_node(self, name):
                if name not in self.fields:
                    raise IndexError(name)
                return self.fields[name]

        # add all the 'base' combinatorial nodes for a given ref, which has type 'vtype'.
        # this returns an aggregate combinatorial node (if the vtype is aggregate) or a
        # non-aggregate combinatorial node (if the vtype is non-aggregate) for the ref.
        def add_nodes_aggregate(ref, vtype, graph, assignment_required=True):
            if isinstance(vtype, UInt) or isinstance(vtype, SInt):
                return graph.add_node(ref, assignment_required)
            elif isinstance(vtype, Vec):
                elems = []
                for i in range(vtype.width):
                    elem_ref = VectorElement(ref, vtype.width, i)
                    elems.append(add_nodes_aggregate(elem_ref, vtype.etype, graph, assignment_required))
                return VectorCombinatorialNode(ref, vtype.width, elems)
            elif isinstance(vtype, Struct):
                fields = {}
                for name in vtype.elems:
                    field_ref = FieldElement(ref, name)
                    fields[name] = add_nodes_aggregate(field_ref, vtype.elems[name], graph, assignment_required)
                return StructCombinatorialNode(ref, fields)
            else:
                print(ref, vtype)
                raise NotImplemented
        # add parallel dependencies (i.e. adding edges to the graph) for potentially
        # aggregate-kinded src and dst combinatorial nodes. use the given ref for every
        # assignment edge.
        # for something like this, the given 'vtype' can be either the src or dst vtype;
        #   it doesn't matter, as long as vector widths agree between the two vtypes.
        def add_parallel_dep(ref, vtype, src, dst, graph):
            if isinstance(vtype, UInt) or isinstance(vtype, SInt):
                graph.add_edge(ref, src, dst)
            elif isinstance(vtype, Vec):
                assert(isinstance(src, VectorCombinatorialNode))
                assert(isinstance(dst, VectorCombinatorialNode))
                for i in range(vtype.width):
                    add_parallel_dep(ref, vtype.etype, src.elems[i], dst.elems[i], graph)
            elif isinstance(vtype, Struct):
                assert(isinstance(src, StructCombinatorialNode))
                assert(isinstance(dst, StructCombinatorialNode))
                for field_name in vtype.elems:
                    add_parallel_dep(ref, vtype.elems[field_name], src.fields[field_name], dst.fields[field_name], graph)
            else:
                print(ref, vtype)
                raise NotImplemented
        # add dependencies (i.e. adding edges to the graph) from a potentially aggregate-kinded
        # src to a potentially aggregate-kinded dst combinatorial node. use the given ref for every
        # assignment edge. instead of performing assignments in parallel, this is more like a product;
        # every basic node in the dst will depend on every basic node in the src.
        # expectation is for this function to be used with either src or dst a basic combinatorial node,
        #   but it does work more generally.
        def add_aggregate_dep(ref, src_vtype, src, dst_vtype, dst, graph):
            if isinstance(dst_vtype, UInt) or isinstance(dst_vtype, SInt):
                # now we have basic dst node; we can add all basic nodes in the src to the dst's deps.
                if isinstance(src_vtype, UInt) or isinstance(src_vtype, SInt):
                    graph.add_edge(ref, src, dst)
                elif isinstance(src_vtype, Vec):
                    assert(isinstance(src, VectorCombinatorialNode))
                    for i in range(src_vtype.width):
                        add_aggregate_dep(ref, src_vtype.etype, src.elems[i], dst_vtype, dst, graph)
                elif isinstance(vtype, Struct):
                    assert(isinstance(src, StructCombinatorialNode))
                    for field_name in range(src_vtype.elems):
                        add_aggregate_dep(ref, src_vtype.elems[field_name], src.fields[field_name])
                else:
                    print(ref, src_vtype)
                    raise NotImplemented
            elif isinstance(dst_vtype, Vec):
                assert(isinstance(dst, VectorCombinatorialNode))
                for i in range(dst_vtype.width):
                    add_aggregate_dep(ref, src_vtype, src, dst_vtype.etype, dst.elems[i], graph)
            elif isinstance(dst_vtype, Struct):
                assert(isinstance(dst, StructCombinatorialNode))
                for field_name in dst_vtype.elems:
                    add_aggregate_dep(ref, src_vtype, src, dst_vtype.elems[field_name], dst.fields[field_name], graph)
            else:
                print(ref, vtype)
                raise NotImplemented


        # add new combinatorial nodes for this exprnode to the graph,
        # along with any edges related to its computation;
        # return the combinatorial node associated to the destination for the exprnode
        def add_node_to_graph(expr, container_instance, graph, component_nodes):
            if isinstance(expr, InputNode) or isinstance(expr, WireNode) or isinstance(expr, RegisterNode) or isinstance(expr, OutputNode):
                component = container_instance.components[expr.name]
                return component_nodes[component]
            elif isinstance(expr, InstanceComponentNode):
                component = container_instance.instances[expr.instance_name].components[expr.component_name]
                return component_nodes[component]
            elif isinstance(expr, DefaultValueNode):
                return graph.add_node(expr, assignment_required=False)
            elif isinstance(expr, LiteralUIntNode):
                return graph.add_node(expr, assignment_required=False)
            elif isinstance(expr, EqNode):
                dst = graph.add_node(expr)
                src_left = add_node_to_graph(expr.left, container_instance, graph, component_nodes)
                src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                #assert(expr.left.vtype == expr.right.vtype)
                src_vtype = expr.left.vtype # equally could use expr.right.vtype
                # we need to add_aggregate_dep instead of just adding edges manually since an EqNode could potentially
                # be comparing values of any types, even aggregate types. the other nodes do not have this property;
                # for example, an AddNode can only have non-aggregate arguments.
                add_aggregate_dep(expr, src_vtype, src_left, expr.vtype, dst, graph)
                add_aggregate_dep(expr, src_vtype, src_right, expr.vtype, dst, graph)
                return dst
            elif isinstance(expr, CatNode):
                if isinstance(expr.vtype, Vec):
                    dst = add_nodes_aggregate(expr, expr.vtype, graph)
                    src_left = add_node_to_graph(expr.left, container_instance, graph, component_nodes)
                    src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                    assert(isinstance(src_left, VectorCombinatorialNode))
                    assert(isinstance(src_right, VectorCombinatorialNode))
                    assert(isinstance(dst, VectorCombinatorialNode))
                    assert(expr.left.vtype == expr.right.vtype)
                    src_etype = expr.left.vtype.etype # can use either expr.left.vtype.etype or expr.right.vtype.etype; they should agree!
                    for i in range(src_left.width):
                        add_parallel_dep(expr, src_etype, src_left.elems[i], dst.elems[i], graph)
                    for i in range(src_right.width):
                        add_parallel_dep(expr, src_etype, src_left.elems[i], dst.elems[i + src_left.width], graph)
                    return dst
                elif isinstance(expr.vtype, UInt) or isinstance(expr.vtype, SInt):
                    dst = graph.add_node(expr)
                    src_left = add_node_to_graph(expr.left, container_instance, graph, component_nodes)
                    src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                    graph.add_edges(expr, [src_left, src_right], dst)
                    return dst
                else:
                    # this probably shouldn't happen, since how can we 'cat' two structs?
                    print(expr)
                    raise NotImplemented
            elif isinstance(expr, MuxNode):
                dst = add_nodes_aggregate(expr, expr.vtype, graph)
                src_sel = add_node_to_graph(expr.sel, container_instance, graph, component_nodes)
                assert(isinstance(src_sel, CombinatorialNode)) # should not be aggregate type!
                add_aggregate_dep(expr, expr.sel.vtype, src_sel, expr.vtype, dst, graph)
                for inp in expr.inputs:
                    src_inp = add_node_to_graph(inp, container_instance, graph, component_nodes)
                    add_parallel_dep(expr, expr.vtype, src_inp, dst, graph)
                return dst
            elif isinstance(expr, SubNode):
                dst = graph.add_node(expr)
                src_left = add_node_to_graph(expr.left, container_instance, graph, component_nodes)
                src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                graph.add_edges(expr, [src_left, src_right], dst)
                return dst
            elif isinstance(expr, AddNode):
                dst = graph.add_node(expr)
                src_left = add_node_to_graph(expr.left, container_instance, graph, component_nodes)
                src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                graph.add_edges(expr, [src_left, src_right], dst)
                return dst
            elif isinstance(expr, LtNode):
                dst = graph.add_node(expr)
                src_left = add_node_to_graph(expr.left, container_instance, graph, component_nodes)
                src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                graph.add_edges(expr, [src_left, src_right], dst)
                return dst
            elif isinstance(expr, LShiftNode):
                dst = graph.add_node(expr)
                src_left = add_node_to_graph(expr.left, container_instance, graph, component_nodes)
                src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                graph.add_edges(expr, [src_left, src_right], dst)
                return dst
            elif isinstance(expr, RShiftNode):
                dst = graph.add_node(expr)
                src_left = add_node_to_graph(expr.left, container_instance, graph, component_nodes)
                src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                graph.add_edges(expr, [src_left, src_right], dst)
                return dst
            elif isinstance(expr, AndNode):
                dst = graph.add_node(expr)
                src_left = add_node_to_graph(expr.left, container_instance, graph, component_nodes)
                src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                graph.add_edges(expr, [src_left, src_right], dst)
                return dst
            elif isinstance(expr, OrNode):
                dst = graph.add_node(expr)
                src_left = add_node_to_graph(expr.left, container_instance, graph, component_nodes)
                src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                graph.add_edges(expr, [src_left, src_right], dst)
                return dst
            elif isinstance(expr, InvertNode):
                dst = graph.add_node(expr)
                src_right = add_node_to_graph(expr.right, container_instance, graph, component_nodes)
                graph.add_edge(expr, src_right, dst)
                return dst
            elif isinstance(expr, IndexNode):
                src_aggregate = add_node_to_graph(expr.aggregate, container_instance, graph, component_nodes)
                if isinstance(expr.index, str):
                    assert(isinstance(src_aggregate, StructCombinatorialNode))
                    return src_aggregate.fields[expr.index]
                elif isinstance(expr.index, int):
                    if isinstance(expr.aggregate.vtype, UInt):
                        assert(isinstance(src_aggregate, CombinatorialNode))
                        # it's a bit-select expr
                        dst = graph.add_node(expr)
                        graph.add_edge(expr, src_aggregate, dst)
                        return dst
                    elif isinstance(expr.aggregate.vtype, Vec):
                        assert(isinstance(src_aggregate, VectorCombinatorialNode))
                        return src_aggregate.elems[expr.index]
                elif isinstance(expr.index, ExprNode):
                    src_index = add_node_to_graph(expr.index, container_instance, graph, component_nodes)
                    # we don't know which index it will be, so we need to assume it can be any possible index.
                    # TODO: we may want to do some static analysis to restrict the range of the index.
                    #   it's not necessary, though; just more accurate. we're being conservative.
                    if isinstance(expr.aggregate.vtype, UInt):
                        # it's a bit-select expr
                        dst = graph.add_node(expr)
                        graph.add_edges(expr, [src_aggregate, src_index], dst)
                        return dst
                    elif isinstance(expr.aggregate.vtype, Vec):
                        assert(isinstance(src_aggregate, VectorCombinatorialNode))
                        dst = add_nodes_aggregate(expr, expr.vtype, graph)
                        add_aggregate_dep(expr, expr.index.vtype, src_index, expr.vtype, dst, graph)
                        vec_type = expr.aggregate.vtype
                        assert(src_aggregate.width == vec_type.width) # sanity
                        for i in range(vec_type.width):
                            add_parallel_dep(expr, vec_type.etype, src_aggregate.elems[i], dst, graph)
                        return dst
                    else:
                        print(expr)
                        raise NotImplemented
                elif isinstance(expr.index, slice):
                    if isinstance(expr.aggregate.vtype, UInt):
                        # it's a (multiple-)bit-select expr
                        dst = graph.add_node(expr)
                        graph.add_edge(expr, src_aggregate, dst)
                        return dst
                    elif isinstance(expr.aggregate.vtype, Vec):
                        if expr.index.step == 0:
                            raise IndexError("Step should not be 0 in expr '%s'!" % expr)
                        vec_len = expr.aggregate.vtype.width
                        start = expr.index.start or 0
                        if start < 0:
                            start += vec_len
                        stop = expr.index.stop or vec_len
                        if stop < 0:
                            stop += vec_len

                        dst = add_nodes_aggregate(expr, expr.vtype, graph)
                        assert(isinstance(src_aggregate, VectorCombinatorialNode)) # sanity
                        assert(isinstance(dst, VectorCombinatorialNode)) # sanity
                        #assert(expr.vtype.etype == expr.aggregate.vtype.etype)
                        vtype = expr.vtype.etype # could equally have picked the expr.aggregate.vtype.etype
                        if expr.index.step is None or expr.index.step > 0:
                            src_i, dst_i = start, 0
                            while src_i < stop:
                                add_parallel_dep(expr, vtype, src_aggregate.elems[src_i], dst.elems[dst_i], graph)
                                src_i += expr.index.step if expr.index.step is not None else 1
                                dst_i += 1
                        else:
                            src_i, dst_i = start, 0
                            while src_i > stop:
                                add_parallel_dep(expr, vtype, src_aggregate.elems[src_i], dst.elems[dst_i], graph)
                                src_i += expr.index.step
                                dst_i += 1
                        return dst
                    else:
                        print(expr)
                        raise NotImplemented
                else:
                    print(expr)
                    raise NotImplemented
            else:
                print(expr)
                raise NotImplemented

        def add_code_to_graph(code, container_instance, graph, component_nodes):
            for node in code:
                if isinstance(node, IfNode):
                    add_node_to_graph(node.cond, container_instance, graph, component_nodes)
                    add_code_to_graph(node.true_branch, container_instance, graph, component_nodes)
                    add_code_to_graph(node.false_branch, container_instance, graph, component_nodes)
                elif isinstance(node, WithNode):
                    add_node_to_graph(node.cond, container_instance, graph, component_nodes)
                    add_code_to_graph(node.body, container_instance, graph, component_nodes)
                elif isinstance(node, AssignCurNode):
                    src = add_node_to_graph(node.src, container_instance, graph, component_nodes)
                    if isinstance(node.dst, InputNode) or isinstance(node.dst, WireNode) or isinstance(node.dst, OutputNode):
                        component = container_instance.components[node.dst.name]
                        dst = component_nodes[component]
                        #assert(node.src.vtype == node.dst.vtype)
                        vtype = node.src.vtype # equally well node.dst.vtype
                        add_parallel_dep(node, vtype, src, dst, graph)
                    elif isinstance(node.dst, RegisterNode):
                        raise Exception("RegisterNode should not be LHS of assignment in '%s'." % node)
                    elif isinstance(node.dst, InstanceComponentNode):
                        component = container_instance.instances[node.dst.instance_name].components[node.dst.component_name]
                        dst = component_nodes[component]
                        #assert(node.src.vtype == node.dst.vtype)
                        vtype = node.src.vtype # equally well node.dst.vtype
                        add_parallel_dep(node, vtype, src, dst, graph)
                    elif isinstance(node.dst, LiteralNode):
                        raise Exception("LiteralNode should not be LHS of assignment in '%s'." % node)
                    elif isinstance(node.dst, ComputationExprNode):
                        raise Exception("ComputationExprNode should not be LHS of assignment in '%s'." % node)
                    elif isinstance(node.dst, IndexNode):
                        dst_aggregate = add_node_to_graph(node.dst.aggregate, container_instance, graph, component_nodes)
                        if isinstance(node.dst.index, str):
                            assert(isinstance(dst_aggregate, StructCombinatorialNode))
                            add_parallel_dep(node, node.dst.vtype, src, dst_aggregate.fields[node.dst.index], graph)
                        elif isinstance(node.dst.index, int):
                            assert(isinstance(dst_aggregate, VectorCombinatorialNode))
                            add_parallel_dep(node, node.dst.vtype, src, dst_aggregate.elems[node.dst.index], graph)
                        elif isinstance(node.dst.index, ExprNode):
                            # add dependencies from src to _every_ element in the dst, since we don't know which index
                            # will actually be assigned.
                            # TODO: static analysis could help reduce the dependencies.
                            assert(isinstance(dst_aggregate, VectorCombinatorialNode))
                            for i in range(dst_aggregate.width):
                                add_parallel_dep(node, node.dst.vtype, src, dst_aggregate.elems[i], graph)
                        elif isinstance(node.dst.index, slice):
                            if isinstance(node.dst.aggregate.vtype, UInt):
                                # it's a (multiple-)bit-select expr
                                graph.add_edge(expr, src, dst_aggregate)
                            elif isinstance(expr.aggregate.vtype, Vec):
                                if node.dst.index.step == 0:
                                    raise IndexError("Step should not be 0 in expr '%s'!" % node.dst)
                                vec_len = node.dst.aggregate.vtype.width
                                start = node.dst.index.start or 0
                                if start < 0:
                                    start += vec_len
                                stop = node.dst.index.stop or vec_len
                                if stop < 0:
                                    stop += vec_len

                                assert(isinstance(src, VectorCombinatorialNode)) # sanity
                                assert(isinstance(dst_aggregate, VectorCombinatorialNode)) # sanity
                                assert(node.dst.vtype.etype == node.src.vtype.etype)
                                vtype = node.dst.vtype.etype # could equally have picked the node.src.vtype.etype
                                if node.dst.index.step is None or node.dst.index.step > 0:
                                    src_i, dst_i = 0, start
                                    while dst_i < stop:
                                        add_parallel_dep(node, vtype, src.elems[src_i], dst_aggregate.elems[dst_i])
                                        src_i += 1
                                        dst_i += node.dst.index.step if node.dst.index.step is not None else 1
                                else:
                                    src_i, dst_i = 0, start
                                    while dst_i > stop:
                                        add_parallel_dep(node, vtype, src.elems[src_i], dst_aggregate.elems[dst_i])
                                        src_i += 1
                                        dst_i += node.dst.index.step
                            else:
                                print(node.dst)
                                raise NotImplemented
                        else:
                            print(node.dst)
                            raise NotImplemented
                    else:
                        print(node.dst)
                        raise NotImplemented
                elif isinstance(node, AssignNextNode):
                    # we actually don't need to do _anything_
                    pass
                    ####add_node_to_graph(node.src, container_instance, graph, component_nodes)
                    #### since it's a .next assignment, which is _not_ combinatorial,
                    #### we do not need edges. (although for funsies, we could add some)
                else:
                    print(node)
                    raise NotImplemented

        # add components, and also components of subinstances, to the
        # graph's .node_mapping.
        # returns a component_nodes mapping!
        def add_components_to_graph(instance, graph, top_level):
            component_nodes = {}
            for name in instance.components:
                component = instance.components[name]
                assignment_required = not ((isinstance(component, InputComponent) and top_level) or isinstance(component, RegisterComponent))
                component_nodes[component] = add_nodes_aggregate(component, component.vtype, graph, assignment_required)
            for subinstance in instance.instances.values():
                more_nodes = add_components_to_graph(subinstance, graph, top_level=False)
                # add to our component_nodes
                for component in more_nodes:
                    component_nodes[component] = more_nodes[component]
            return component_nodes

        # add instance combinatorial structure, along with all that of
        # its subinstances, to the graph
        def add_instance_to_graph(instance, graph, component_nodes):
            add_code_to_graph(instance.code, instance, graph, component_nodes)
            for subinstance in instance.instances.values():
                add_instance_to_graph(subinstance, graph, component_nodes)

        graph = Graph()
        component_nodes = add_components_to_graph(self, graph, top_level=True)
        add_instance_to_graph(self, graph, component_nodes)

        def topologically_sort(graph):
            # the nodes already know their dependencies
            nodes = list(graph.nodes)
            top_sort = []
            # invariant: no node in `nodes` depends on any node in `top_sort`
            while nodes:
                nodes_without_deps = [node for node in nodes if not node.deps]
                # if every node has dependencies, there's a cycle in the remaining nodes!
                if not nodes_without_deps:
                    cycle = None
                    path = [nodes[0]]
                    cur = path[0]
                    while True:
                        next_node = list(cur.deps)[0] # somewhat randomly pick a dependency
                        if next_node in path:
                            index = path.index(next_node)
                            cycle = path[index:]
                            cycle.append(next_node)
                            break
                        else:
                            path.append(next_node)
                            cur = next_node
                    print("There is a combinatorial cycle.")
                    print("Here is an example:")
                    for i in range(len(cycle) - 1):
                        node, dep = cycle[i], cycle[i + 1]
                        matching_edge = None
                        for edge in graph.edges:
                            if edge.src == dep and edge.dst == node:
                                matching_edge = edge
                                break
                        print("   <%s> depends on <%s> (from '%s')" % (node.ref, dep.ref, edge.ref))
                    raise TypeError("There is a combinatorial cycle.")
                else:
                    # there are nodes without dependencies!
                    top_sort.extend(nodes_without_deps)
                    for src in nodes_without_deps:
                        for dst in src.dep_of:
                            dst.deps.remove(src)
                        nodes.remove(src)
            return top_sort

        # steps 2, 3: attempt to topologically sort the graph,
        # and print a cycle if not possible
        top_sort = topologically_sort(graph)

        # now find an edge ordering based on the top_sort of nodes,
        # so that the edge.dst's are in top_sort order (with possible repeats, of course)
        edges_with_dst = {}
        for edge in graph.edges:
            if edge.dst in edges_with_dst:
                edges_with_dst[edge.dst].append(edge)
            else:
                edges_with_dst[edge.dst] = [edge]
        edge_ordering = []
        for dst in top_sort:
            if dst in edges_with_dst:
                edge_ordering.extend(edges_with_dst[dst])
            else:
                if dst.assignment_required:
                    print("WARNING: '%s' is never assigned." % (dst.ref))

        self.verified_no_cycles = True

        # TODO: just for debug; remove later
        self.combinatorial_graph = graph
        self.component_nodes = component_nodes
        self.edge_ordering = edge_ordering

    # converts the code of this instance (but not any subinstances) to a lowered, more simplified form;
    # returns the new code
    def lower_code(self):
        def convert_code(code, guard):
            def add_guard(cond):
                return cond.copy() if guard is None else AndNode(guard, cond.copy())
            new_code = []
            for node in code:
                if isinstance(node, IfNode):
                    true_guard = add_guard(node.cond)
                    lowered_true_branch = convert_code(node.true_branch, true_guard)
                    false_guard = add_guard(InvertNode(node.cond))
                    lowered_false_branch = convert_code(node.false_branch, false_guard)
                    new_code.extend(lowered_true_branch)
                    new_code.extend(lowered_false_branch)
                elif isinstance(node, WithNode):
                    body_guard = add_guard(node.cond)
                    lowered_body = convert_code(node.body, body_guard)
                    new_code.extend(lowered_body)
                elif isinstance(node, AssignCurNode):
                    # TODO: this is definitely not right lol
                    if guard is not None:
                        new_code.append(AssignCurNode(node.dst.copy(), MuxNode(guard, [node.src.copy(), node.dst.copy()], node.src.vtype.copy())))
                    else:
                        new_code.append(node.copy())
                elif isinstance(node, AssignNextNode):
                    # TODO: this is definitely not right lol
                    if guard is not None:
                        new_code.append(AssignNextNode(node.dst.copy(), MuxNode(guard, [node.src.copy(), node.dst.copy()], node.src.vtype.copy())))
                    else:
                        new_code.append(node.copy())
                else:
                    print(node)
                    raise NotImplemented
            return new_code
        return convert_code(self.code, guard=None) 

    # lowers this instance's code, as well as that of all its subinstances.
    # returns a new ModuleInstance with the new code (for all subinstances).
    def fully_lowered_form(self):
        lowered_instances = { name: self.instances[name].fully_lowered_form() for name in self.instances }
        lowered_code = self.lower_code()
        # TODO: copy width-inferred and has-no-cycles flags?
        return ModuleInstance(self.module_name, self.name, dict(self.components), lowered_code, lowered_instances)

    def print_interface(self, cur_indent=""):
        inputs, outputs = {}, {}
        for name in self.components:
            component = self.components[name]
            if isinstance(component, InputComponent):
                inputs[name] = component
            elif isinstance(component, WireComponent):
                pass
            elif isinstance(component, RegisterComponent):
                pass
            elif isinstance(component, OutputComponent):
                outputs[name] = component
            else:
                raise Exception()
        for name in inputs:
            print(cur_indent + "input %s: %s" % (name, inputs[name].vtype))
        for name in outputs:
            print(cur_indent + "output %s: %s" % (name, outputs[name].vtype))

    def print_components(self, cur_indent=""):
        inputs, wires, regs, outputs = {}, {}, {}, {}
        for name in self.components:
            component = self.components[name]
            if isinstance(component, InputComponent):
                inputs[name] = component
            elif isinstance(component, WireComponent):
                wires[name] = component
            elif isinstance(component, RegisterComponent):
                regs[name] = component
            elif isinstance(component, OutputComponent):
                outputs[name] = component
            else:
                raise Exception()
        for name in inputs:
            print(cur_indent + "input %s: %s" % (name, inputs[name].vtype))
        for name in regs:
            reg = regs[name]
            print(cur_indent + "reg %s: %s, reset=%s" % (name, reg.vtype, reg.reset))
        for name in wires:
            print(cur_indent + "wire %s: %s" % (name, wires[name].vtype))
        for name in outputs:
            print(cur_indent + "output %s: %s" % (name, outputs[name].vtype))

    def pprint(self, depth=0, indent="  ", cur_indent=""):
        print(cur_indent + "instance %s of %s:" % (self.name, self.module_name))
        cur_indent += indent
        if self.components:
            self.print_components(cur_indent)
            print(cur_indent)

        def pprint_code(code, indent, cur_indent):
            if not code:
                print(cur_indent + "pass")
            else:
                for node in code:
                    if isinstance(node, IfNode):
                        print(cur_indent + "if %s:" % node.cond)
                        pprint_code(node.true_branch, indent, cur_indent + indent)
                        if node.false_branch:
                            print(cur_indent + "else:")
                            pprint_code(node.false_branch, indent, cur_indent + indent)
                    elif isinstance(node, WithNode):
                        print(cur_indent + "with %s:" % node.cond)
                        pprint_code(node.body, indent, cur_indent + indent)
                    else:
                        print(cur_indent + str(node))

        pprint_code(self.code, indent, cur_indent)

        if depth is None or depth > 0:
            for instance_name in self.instances:
                instance = self.instances[instance_name]
                print(cur_indent)
                instance.pprint(None if depth is None else depth - 1, indent, cur_indent)

class CompiledModule:
    def __init__(self, name, components, code, thin_instances):
        self.name = name
        self.components = components
        self.code = code
        self.thin_instances = thin_instances

        for name in self.thin_instances:
            instance = self.thin_instances[name]
            assert(isinstance(instance, ThinModuleInstance))

        self.typechecked = False

    def as_full_instance(self, instance_name=None):
        assert(self.typechecked)
        return ThinModuleInstance(instance_name if instance_name else self.name, self).as_full_instance()
        
    # infer any necessary types for the current module and all of its submodules;
    # also typecheck any explicitly given types
    def typecheck(self):
        set_structural_equality()
        # TODO: typecheck (and infer widths) also reset values for regs!

        # we can typecheck first, and then infer all widths

        # calculate the type of the given ExprNode,
        # and set its .vtype; raise a type error if no type
        # could be inferred. return the calculated vtype.
        # this is allowed to specify width=Q; it need not calculate
        # any width, since width inference will do that later.
        # this method will set the .readable, .cur_writeable, and
        #   .next_writeable flags for the node type.
        # the container_module is the CompiledModule whose body (i.e. .code)
        #   contains the given node
        def infer_type(node, container_module):
            try:
                #print("inferring type of '%s'" % node)
                if isinstance(node, InputNode):
                    node.vtype.readable = True
                elif isinstance(node, WireNode):
                    node.vtype.readable = True
                    node.vtype.cur_writeable = True
                elif isinstance(node, RegisterNode):
                    node.vtype.readable = True
                    node.vtype.next_writeable = True
                elif isinstance(node, OutputNode):
                    node.vtype.readable = True
                    node.vtype.cur_writeable = True
                elif isinstance(node, InstanceComponentNode):
                    #infer_type(node.component, container_module)
                    #node.vtype = node.component.vtype.copy()
                    instance = container_module.thin_instances[node.instance_name]
                    component = instance.module.components[node.component_name]
                    node.vtype = component.vtype.copy()
                    if isinstance(component, InputComponent):
                        node.vtype.cur_writeable = True
                    elif isinstance(component, OutputComponent):
                        node.vtype.readable = True
                    else:
                        raise TypeError("Cannot access internal wires or registers of instance '%s' in the expression '%s'." % (node.instance_name, node))
                elif isinstance(node, DefaultValueNode):
                    # already has vtype!
                    node.vtype.readable = True
                    node.vtype.cur_writeable = False
                    node.vtype.next_writeable = False
                elif isinstance(node, LiteralUIntNode):
                    node.vtype = UInt(node.width)
                    node.vtype.readable = True
                elif isinstance(node, EqNode):
                    infer_type(node.left, container_module)
                    ensure_readable(node.left)
                    infer_type(node.right, container_module)
                    ensure_readable(node.right)
                    if isinstance(node.left.vtype, UInt) and isinstance(node.right.vtype, UInt):
                        node.vtype = UInt(1)
                        node.vtype.readable = True
                    else:
                        # TODO: allow comparing any values of the same type?
                        raise TypeError("Cannot compare values of type %s and %s in the expression '%s'." % (node.left.vtype, node.right.vtype, node))
                elif isinstance(node, CatNode):
                    infer_type(node.left, container_module)
                    ensure_readable(node.left)
                    infer_type(node.right, container_module)
                    ensure_readable(node.right)
                    if isinstance(node.left.vtype, UInt) and isinstance(node.right.vtype, UInt):
                        node.vtype = UInt()
                        node.vtype.readable = True
                    #elif isinstance(node.left.vtype, Vec) and isinstance(node.right.vtype, Vec) and : TODO compare etypes
                    #    node.vtype = Vec(...)
                    #    node.vtype.readable = True
                    else:
                        raise TypeError("Cannot concatenate values of type %s and %s in the expression '%s'." % (node.left.vtype, node.right.vtype, node))
                elif isinstance(node, MuxNode):
                    infer_type(node.sel, container_module)
                    ensure_readable(node.sel)
                    for inp in node.inputs:
                        infer_type(inp, container_module)
                        ensure_readable(inp)
                    if len(node.inputs) < 2:
                        raise TypeError("Mux must have at least two options in the expression '%s'." % node)
                    if isinstance(node.sel.vtype, UInt):
                        # we're not checking the selector width here
                        input_type = infer_type(node.inputs[0], container_module)
                        for inp in node.inputs[1:]:
                            ensure_type(inp, input_type, container_module)
                        node.vtype = input_type.copy()
                    else:
                        raise TypeError("Selector of mux must be a UInt instead of %s in the expression '%s'." % (node.sel.vtype, node))
                elif isinstance(node, SubNode):
                    infer_type(node.left, container_module)
                    ensure_readable(node.left)
                    infer_type(node.right, container_module)
                    ensure_readable(node.right)
                    if isinstance(node.left.vtype, UInt) and isinstance(node.right.vtype, UInt):
                        node.vtype = UInt()
                        node.vtype.readable = True
                    else:
                        raise TypeError("Cannot subtract values of type %s and %s in the expression '%s'." % (node.left.vtype, node.right.vtype, node))
                elif isinstance(node, AddNode):
                    infer_type(node.left, container_module)
                    ensure_readable(node.left)
                    infer_type(node.right, container_module)
                    ensure_readable(node.right)
                    if isinstance(node.left.vtype, UInt) and isinstance(node.right.vtype, UInt):
                        node.vtype = UInt()
                        node.vtype.readable = True
                    else:
                        raise TypeError("Cannot add values of type %s and %s in the expression '%s'." % (node.left.vtype, node.right.vtype, node))
                elif isinstance(node, LtNode):
                    infer_type(node.left, container_module)
                    ensure_readable(node.left)
                    infer_type(node.right, container_module)
                    ensure_readable(node.right)
                    if isinstance(node.left.vtype, UInt) and isinstance(node.right.vtype, UInt):
                        node.vtype = UInt(1)
                        node.vtype.readable = True
                    else:
                        raise TypeError("Cannot compare values of type %s and %s in the expression '%s'." % (node.left.vtype, node.right.vtype, node))
                elif isinstance(node, LShiftNode):
                    infer_type(node.left, container_module)
                    ensure_readable(node.left)
                    infer_type(node.right, container_module)
                    ensure_readable(node.right)
                    if isinstance(node.left.vtype, UInt) and isinstance(node.right.vtype, UInt):
                        node.vtype = UInt()
                        node.vtype.readable = True
                    else:
                        raise TypeError("Cannot left-shift value of type %s by a value of type %s in the expression '%s'." % (node.left.vtype, node.right.vtype, node))
                elif isinstance(node, RShiftNode):
                    infer_type(node.left, container_module)
                    ensure_readable(node.left)
                    infer_type(node.right, container_module)
                    ensure_readable(node.right)
                    if isinstance(node.left.vtype, UInt) and isinstance(node.right.vtype, UInt):
                        node.vtype = UInt()
                        node.vtype.readable = True
                    else:
                        raise TypeError("Cannot right-shift value of type %s by a value of type %s in the expression '%s'." % (node.left.vtype, node.right.vtype, node))
                elif isinstance(node, AndNode):
                    infer_type(node.left, container_module)
                    ensure_readable(node.left)
                    infer_type(node.right, container_module)
                    ensure_readable(node.right)
                    if isinstance(node.left.vtype, UInt) and isinstance(node.right.vtype, UInt):
                        node.vtype = UInt()
                        node.vtype.readable = True
                    else:
                        raise TypeError("Cannot bitwise-and values of type %s and %s in the expression '%s'." % (node.left.vtype, node.right.vtype, node))
                elif isinstance(node, OrNode):
                    infer_type(node.left, container_module)
                    ensure_readable(node.left)
                    infer_type(node.right, container_module)
                    ensure_readable(node.right)
                    if isinstance(node.left.vtype, UInt) and isinstance(node.right.vtype, UInt):
                        node.vtype = UInt()
                        node.vtype.readable = True
                    else:
                        raise TypeError("Cannot bitwise-or values of type %s and %s in the expression '%s'." % (node.left.vtype, node.right.vtype, node))
                elif isinstance(node, InvertNode):
                    infer_type(node.right, container_module)
                    ensure_readable(node.right)
                    if isinstance(node.right.vtype, UInt):
                        node.vtype = UInt()
                        node.vtype.readable = True
                    else:
                        raise TypeError("Cannot bitwise-invert a value of type %s in the expression '%s'." % (node.right.vtype, node))
                elif isinstance(node, IndexNode):
                    infer_type(node.aggregate, container_module)
                    # node.index could be an int or str or ExprNode,
                    # or it could be a slice whose .start, .stop, .step are ints
                    if isinstance(node.index, int):
                        if isinstance(node.aggregate.vtype, UInt):
                            node.vtype = UInt(1)
                        elif isinstance(node.aggregate.vtype, Vec):
                            node.vtype = node.aggregate.vtype.etype.copy()
                        else:
                            raise TypeError("Cannot index a value of type %s in the expression '%s': UInt or Vec is required." % (node.aggregate, node))
                    elif isinstance(node.index, str):
                        if isinstance(node.aggregate.vtype, Struct):
                            if node.index in node.aggregate.vtype.elems:
                                node.vtype = node.aggregate.vtype.elems[node.index].copy()
                            else:
                                raise TypeError("Key '%s' does not exist in struct '%s' in the expression '%s'." % (node.index, node.aggregate, node))
                        else:
                            raise TypeError("Cannot select from a value of type %s in the expression '%s': a Struct is required." % (node.aggregate, node))
                    if isinstance(node.index, ExprNode):
                        infer_type(node.index, container_module)
                        ensure_type(node.index, UInt, container_module)
                        if isinstance(node.aggregate.vtype, UInt):
                            node.vtype = UInt(1)
                        elif isinstance(node.aggregate.vtype, Vec):
                            node.vtype = node.aggregate.vtype.etype.copy()
                        else:
                            raise TypeError("Cannot index a value of type %s in the expression '%s': UInt or Vec is required." % (node.aggregate, node))
                    elif isinstance(node.index, slice):
                        if not (node.index.start is None or isinstance(node.index.start, int)):
                            raise TypeError("Start value of slice in the expression '%s' must be an integer." % node)
                        if not (node.index.stop is None or isinstance(node.index.stop, int)):
                            raise TypeError("Stop value of slice in the expression '%s' must be an integer." % node)
                        if not (node.index.step is None or isinstance(node.index.step, int)):
                            raise TypeError("Step value of slice in the expression '%s' must be an integer." % node)
                        if isinstance(node.aggregate.vtype, UInt):
                            node.vtype = UInt()
                        elif isinstance(node.aggregate.vtype, Vec):
                            node.vtype = Vec(node.aggregate.vtype.etype.copy())
                        else:
                            raise TypeError("Cannot slice a value of type %s in the expression '%s': UInt or Vec is required." % (node.aggregate, node))
                    node.vtype.readable = node.aggregate.vtype.readable
                    node.vtype.cur_writeable = node.aggregate.vtype.cur_writeable
                    node.vtype.next_writeable = node.aggregate.vtype.next_writeable
                else:
                    print("cannot infer type for node '%s'" % node)
                    raise NotImplemented
                #print("inferred type of '%s' is %s{%s%s%s}" % (node, node.vtype,
                #    "R" if node.vtype.readable else "r", "C" if node.vtype.cur_writeable else "c", "N" if node.vtype.next_writeable else "n"))
                return node.vtype
            except TypeError as e:
                print("In expression '%s'." % node)
                raise e

        # infer the node's type, and verify that it is the given
        # type. vtype may be either a type instantiated with a certain width,
        # or it could be a width-generic type; for example,
        #    ensure_type(node, UInt)
        # makes sure that node.vtype is a UInt of any width, while
        #    ensure_type(node, UInt(10))
        # ensures that node.vtype is a UInt of width Q or 10.
        def ensure_type(node, vtype, container_module):
            def type_str(vtype):
                if vtype is UInt:
                    return "UInt"
                elif isinstance(vtype, UInt):
                    return str(vtype)
                elif vtype is Vec:
                    return "Vec"
                elif isinstance(vtype, Vec):
                    return str(vtype)
                else:
                    raise NotImplemented
            def types_agree(vtype, actual):
                if vtype is UInt:
                    return isinstance(actual, UInt)
                elif isinstance(vtype, UInt):
                    return isinstance(actual, UInt) and (actual.width is Q or vtype.width is Q or actual.width == vtype.width)
                elif vtype is Vec:
                    return isinstance(actual, Vec)
                elif isinstance(vtype, Vec):
                    if not isinstance(actual, Vec) and (actual.width is Q or vtype.width is Q or actual.width == vtype.width):
                        return False
                    return types_agree(vtype.etype, actual.etype)
                elif isinstance(vtype, Struct):
                    if not isinstance(actual, Struct):
                        return False
                    if not set(vtype.elems.keys()) == set(actual.elems.keys()):
                        return False
                    return all(types_agree(vtype.elems[name], actual.elems[name]) for name in vtype.elems)
                else:
                    raise NotImplemented

            actual = infer_type(node, container_module)
            if not types_agree(vtype, actual):
                raise TypeError("Expected `%s` to have type %s, but it has type %s." % (node, type_str(vtype), actual))

        def ensure_readable(node):
            if not node.vtype.readable:
                raise TypeError("`%s` is not readable." % node)

        # typecheck the code of the module, and then recursively typecheck the submodules
        def typecheck_module(module):
            def typecheck_codeseq(code, container_module):
                for node in code:
                    if isinstance(node, IfNode):
                        ensure_type(node.cond, UInt(1), container_module)
                        ensure_readable(node.cond)
                        typecheck_codeseq(node.true_branch, container_module)
                        typecheck_codeseq(node.false_branch, container_module)
                    elif isinstance(node, WithNode):
                        ensure_type(node.cond, UInt(1), container_module)
                        ensure_readable(node.cond)
                        typecheck_codeseq(node.body, container_module)
                    elif isinstance(node, AssignCurNode):
                        try:
                            dst_type = infer_type(node.dst, container_module)
                            src_type = infer_type(node.src, container_module)
                            if not dst_type.cur_writeable:
                                raise TypeError("Cannot assign to `%s.cur` in the assignment `%s`; the current value is not writeable." % (node.dst, node))
                            ensure_readable(node.src)
                            ensure_type(node.dst, src_type, container_module)
                        except TypeError as e:
                            print("In assignment '%s'." % node)
                            raise e
                    elif isinstance(node, AssignNextNode):
                        try:
                            dst_type = infer_type(node.dst, container_module)
                            src_type = infer_type(node.src, container_module)
                            if not dst_type.next_writeable:
                                raise TypeError("Cannot assign to `%s.next` in the assignment `%s`; the next value is not writeable." % (node.dst, node))
                            ensure_readable(node.src)
                            ensure_type(node.dst, src_type, container_module)
                        except TypeError as e:
                            print("In assignment '%s'." % node)
                            raise e
                    else:
                        raise NotImplemented

            typecheck_codeseq(module.code, module)
            for instance in module.thin_instances.values():
                typecheck_module(instance.module)
            module.typechecked = True

        typecheck_module(self)

    def print_interface(self, cur_indent=""):
        inputs, outputs = {}, {}
        for name in self.components:
            component = self.components[name]
            if isinstance(component, InputComponent):
                inputs[name] = component
            elif isinstance(component, WireComponent):
                pass
            elif isinstance(component, RegisterComponent):
                pass
            elif isinstance(component, OutputComponent):
                outputs[name] = component
            else:
                raise Exception()
        for name in inputs:
            print(cur_indent + "input %s: %s" % (name, inputs[name].vtype))
        for name in outputs:
            print(cur_indent + "output %s: %s" % (name, outputs[name].vtype))

    def print_components(self, cur_indent=""):
        inputs, wires, regs, outputs = {}, {}, {}, {}
        for name in self.components:
            component = self.components[name]
            if isinstance(component, InputComponent):
                inputs[name] = component
            elif isinstance(component, WireComponent):
                wires[name] = component
            elif isinstance(component, RegisterComponent):
                regs[name] = component
            elif isinstance(component, OutputComponent):
                outputs[name] = component
            else:
                raise Exception()
        for name in inputs:
            print(cur_indent + "input %s: %s" % (name, inputs[name].vtype))
        for name in regs:
            reg = regs[name]
            print(cur_indent + "reg %s: %s, reset=%s" % (name, reg.vtype, reg.reset))
        for name in wires:
            print(cur_indent + "wire %s: %s" % (name, wires[name].vtype))
        for name in outputs:
            print(cur_indent + "output %s: %s" % (name, outputs[name].vtype))

    def pprint(self, depth=0, indent="  ", cur_indent="", instance_name=None):
        if instance_name is None:
            print(cur_indent + "module %s:" % self.name)
        else:
            print(cur_indent + "instance %s of %s:" % (instance_name, self.name))
        cur_indent += indent
        if self.components:
            self.print_components(cur_indent)
            print(cur_indent)

        def pprint_code(code, indent, cur_indent):
            if not code:
                print(cur_indent + "pass")
            else:
                for node in code:
                    if isinstance(node, IfNode):
                        print(cur_indent + "if %s:" % node.cond)
                        pprint_code(node.true_branch, indent, cur_indent + indent)
                        if node.false_branch:
                            print(cur_indent + "else:")
                            pprint_code(node.false_branch, indent, cur_indent + indent)
                    elif isinstance(node, WithNode):
                        print(cur_indent + "with %s:" % node.cond)
                        pprint_code(node.body, indent, cur_indent + indent)
                    else:
                        print(cur_indent + str(node))

        pprint_code(self.code, indent, cur_indent)

        if depth is None or depth > 0:
            for instance_name in self.thin_instances:
                thin_instance = self.thin_instances[instance_name]
                print(cur_indent)
                thin_instance.module.pprint(None if depth is None else depth - 1, indent, cur_indent, instance_name)
